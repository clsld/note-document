类加载原理

只要有三个步骤：加载、连接、初始化，连接又可以分为验证、准备、解析。



加载就是将类的.class文件中的二进制数据读入内存中，将其放在方法区内，然后在堆去创建一个java.lang.Class对象，用来封装类在方法区内的数据结构。类的加载的最终产品是位于堆区中的Class对象，Class对象封装了类在方法区内的数据结构，并且向Java程序员提供了访问方法区内的数据结构的接口。

> 方法区里面存放着class文件的信息和运行时常量池，class文件的信息包括类信息和class文件常量池
>
> class文件常量池存储的是当class文件被java虚拟机加载进来后存放在方法区的一些字面量和符号引用。
>
> 字面量包含文本字符串、被声明为final的常量、基本数据类型的值；字符引用包括类和接口的全限定名、字段名称和描述符、方法名称和描述符。
>
> 运行时常量池里的内容除了是class文件常量池里的内容外，还将class文件常量池里的符号引用转变为直接引用，而运行时常量池里的内容是能动态添加的（运行时常量池相对于class文件常量池的另外一个重要特征是具备动态性，不要求常量一定只有编译期才能产生，运行期间也可能将新的常量放入池中，例如String的intern方法）。Jdk 1.8后将String常量池放到了堆中，之前是包含在运行时常量池里的。
>
> 常量池是为了避免频繁的创建和销毁对象而影响系统性能，其实现了对象的共享。（1）节省内存空间：常量池中所有相同的字符串常量被合并，只占用一个空间。（2）节省运行时间：比较字符串时，==比equals()快。对于两个引用变量，只用==判断引用是否相等，也就可以判断实际值是否相等。
>
> 还有说法是没将池放入方法区的：方法区中存放的是类型信息、常量、静态变量、即时编译器编译后的代码缓存、域信息、方法信息等。



验证包括对文件格式的验证（验证字节流是否符合class文件格式的规范，常量池中的常量是否有不被支持的类型）、元数据的验证（对字节码描述的信息进行语义分析，看某些类是否被继承了被允许被继承的类（如被final类修饰的类））、字节码验证（通过数据流和控制流分析，确定程序语义是否合法的，符合逻辑的）、符号引用验证（确保解析动作能正常执行）



准备是正式为类变量分配内存并设置类变量初始化的阶段，这些内存都建在方法区中分配。例如我们定义了public static int value = 111，那么value变量在准备阶段的初始值是0而不是111（初始化阶段才会赋值）。但是如果给value变量加上了final关键字即public static final int value = 111，那么准备阶段value的值就会被赋值为111。



解析阶段是虚拟机将常量池内的符号引用替换为直接引用的过程。解析动作主要针对类或接口、字段、类方法、接口方法、方法类型、方法句柄和调用限定符七类符号进行引用进行。符号引用就是一组符号来描述目标，可以是任何字面量。直接引用就是直接指向目标的指针、相对偏移量或一个间接定位到目标的句柄。在程序执行方法时，系统需要明确知道这个方法所在的位置。Java 虚拟机为每个类都准备了一张方法表来存放类中所有的方法。当需要调用一个类的方法的时候，只要知道这个方法在方发表中的偏移量就可以直接调用该方法了。通过解析操作符号引用就可以直接转变为目标方法在类中方法表的位置，从而使得方法可以被调用。



初始化时类加载的最后一步，也是真正执行类中定义的字节码的过程。初始化阶段是执行类构造器 `<clinit> ()`方法的过程。

> 对于初始化阶段，虚拟机严格规范了有且只有5中情况下，必须对类进行初始化：
>
> 1. 当遇到 new 、 getstatic、putstatic或invokestatic 这4条直接码指令时，比如 new 一个类，读取一个静态字段(未被 final 修饰)、或调用一个类的静态方法时。
> 2. 使用 `java.lang.reflect` 包的方法对类进行反射调用时 ，如果类没初始化，需要触发其初始化。
> 3. 初始化一个类，如果其父类还未初始化，则先触发该父类的初始化。
> 4. 当虚拟机启动时，用户需要定义一个要执行的主类 (包含 main 方法的那个类)，虚拟机会先初始化这个类。
> 5. 当使用 JDK1.7 的动态动态语言时，如果一个 MethodHandle 实例的最后解析结构为 REF_getStatic、REF_putStatic、REF_invokeStatic、的方法句柄，并且这个句柄没有初始化，则需要先触发器初始化。





s



实习项目

> 侵入影响
>
> 性能影响
>
> 数据存储
>
> aop开启配置

学校项目

Mysql底层索引

快速排序

基本数据类型

spring事务

@Autowired和@Resource的区别

注解懒加载







接口和抽象类的区别

接口

接口里面的变量默认隐含类型是`public static final`，即就是常量，而默认方法的类型是`public abstract`,所以接口的方法都是抽象方法

default方法：

定义接口进行实践，default声明的方法需要在接口中就进行实现

```java
public interface MytestInterface extends Iterable{

    void getType();

    default void getName(String values){
        System.out.println("new getName");
    }
}
```

我们都知道接口是定义的方法那么implement它的类就要实现他的方法，但是那么这样子的话如果要在接口中新增一个方法，那么之前实现了这个接口的方法就会报错，这就会导致我们还要对之前实现了的类进行重现实现，但是有时候这个类又不是一定需要这个方法，那么这时候default声明的方法就起到了作用。

`InterfaceImpl`中实现接口，可以看到可以不用实现在接口中定义的default方法

```java
public class InterfaceImpl implements MytestInterface {



    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public void forEach(Consumer action) {
        MytestInterface.super.forEach(action);
    }

    @Override
    public Spliterator spliterator() {
        return MytestInterface.super.spliterator();
    }

    @Override
    public void getType() {
        System.out.println("InterfaceImpl getType");
    }
}
```

在`InterfaceImpl1`中实现接口方法，看出也可以对default方法进行实现

```java
public class InterfaceImpl1 implements MytestInterface {



    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public void forEach(Consumer action) {
        MytestInterface.super.forEach(action);
    }

    @Override
    public Spliterator spliterator() {
        return MytestInterface.super.spliterator();
    }

    @Override
    public void getType() {
        System.out.println("InterfaceImpl1 getType");
    }

    @Override
    public void getName(String values) {
        MytestInterface.super.getName(values);
    }
}
```

在main方法中调用看结果

```java
public class TestHashMapDemo{
    public static void main(String[] args) {
        InterfaceImpl anInterface = new InterfaceImpl();
        InterfaceImpl1 anInterface1 = new InterfaceImpl1();
        //对InterfaceImpl中实现的getType接口进行调用
        anInterface.getType();
        //对InterfaceImpl中未实现的getName接口进行调用
        anInterface.getName("1");
        //对InterfaceImpl1中实现的getType接口进行调用
        anInterface1.getType();
        //对InterfaceImpl1中实现的getName接口进行调用
        anInterface1.getName("2");
    }
}
```

结果为

```shell
InterfaceImpl getType
new getName
InterfaceImpl1 getType
new getName
```

它可以使类不用实现这个default方法声明的接口方法，当然类也可以实现它对它进行重写，就算不对他进行重写也可以对原本接口中default声明的方法进行调用。



抽象类和接口的本质区别

抽象类可以有构造方法，接口中不能有构造方法

抽象类可以有任何类型的成员变量，接口中只能有`public static final`变量

抽象类中可以包括非抽象的普通方法，接口中也可以有非抽象的`default`方法

抽象类中的抽象方法的访问类型可以是`public protected等`，但是接口中的抽象方法只能是`public`类型，并且默认为`public abstract`类型

抽象类中可以包含静态方法，接口中不能包含静态方法

抽象类和接口中都可以包含静态成员变量，抽象类中的静态成员变量的访问类型可以任意，但是接口中定义的变量只能是也默认是`public static final`类型

一个类可以实现多个接口，但只能继承一个抽象类

抽象类

抽象类是不能实例化的，但是特能够通过在继承他的子类中调用他的构造方法来达到实例化的作用，因为子类在实例化的时候会实例化父类，所以子类才能调用父类的构造方法

策略模式

```java
interface A{
    void listen();
}
class Man implements A{
    @Override
    public void listen(){
        sout("Man listen");
    }
}
class Woman implements A{
    @Override
    public void listen(){
        sout("Woman listen");
    }
}

public class test{
    //将接口作为参数进行调用
    public static void doListen(A a){
        a.listen();
    }
    
    public static void main(String[] args){
        listen(new Man);
        listen(new Woman);
    }
}
```

适配器模式

```java
interface A{
    void listen();
    void watch();
}

//通过抽象类实现接口方法使继承了抽象类的方法不用全部实现接口的方法
abstract class AdapterA implements A{
    public void listen(){}
    public void watch(){}
}

//继承了抽象类的子类可以重写方法选择性实现接口方法
class Man extends AdapterA{
    public void listen(){
        sout("man listen");
    }
}

public class test{
    public static void main(String[] args){
        A man = new Man();
        man.listen();
    }
}
```

工厂模式

```java
interface A{
    void play();
}
//通过新增一个工厂接口来进行创建A的功能
interface AFactory{
    A creatA();
}
//创建不用类型的对象实现A的功能
class Man implements A{
    @Override
    public void play(){
        sout("play Man")
    }
}
class Woman implements A{
    @Override
    public void play(){
        sout("play Woman")
    }
}

//创建工厂对象实现工厂接口方法创建对象
class ManFactory implements AFactory{
    @Override
    public A creatA(){
        return new Man();
    }
}

class WomanFactory implements AFactory{
    @Override
    public A creatB(){
        return new Woman();
    }
}

//传入不同的工厂对象来创建对象、
public class test{
    public static void create(AFactory factory){
        factory.creatA.play();
    }
    public static void main(Stirng[] args){
        create(new ManFactory);
        create(new WomanFactory);
    }
}
```



ArrayList底层

char和varchar

> 一个字节为8个bit为，ASCII码中，一个英文字母（不分大小写）占一个字节的空间，一个中文汉字占两个字节的空间。UTF-8编码中，一个英文字符等于一个字节，一个中文（含繁体）等于三个字节。Unicode编码中，一个英文等于两个字节，一个中文（含繁体）等于两个字节。
>
> char使用指定长度来表示字符串的一种字段类型，比如char(8)那么数据库会使用固定的一个字节(八位)来存储数据，不足8位的字符串在其后补空字符；
>
> varchar是一种比char更加灵活的数据类型，同样用于表示字符数据，但是varchar可以保存可变长度的字符串。









垃圾回收过程
voliactive实现机制
ReentrantLock和hashMap实现原来
MySQL底层索引原理，为什么子节点不存储数据
Redis数据结构的运用





















