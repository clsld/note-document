ReentrantLock是Lock的实现类，Lock接口如下：

```java
public interface Lock {
    //加锁
    void lock();
    //解锁
    void unlock();
    //可中断获取锁，与lock()不同之处在于可响应中断操作，即在获
    //取锁的过程中可中断，注意synchronized在获取锁时是不可中断的
    void lockInterruptibly() throws InterruptedException;
    //尝试非阻塞获取锁，调用该方法后立即返回结果，如果能够获取则返回true，否则返回false
    boolean tryLock();
    //根据传入的时间段获取锁，在指定时间内没有获取锁则返回false，如果在指定时间内当前线程未被中并断获取到锁则返回true
    boolean tryLock(long time, TimeUnit unit) throws InterruptedException;
    //获取等待通知组件，该组件与当前锁绑定，当前线程只有获得了锁
    //才能调用该组件的wait()方法，而调用后，当前线程将释放锁。
    Condition newCondition();
}
```

Lock对象说还提供了synchronized所不具备的其他同步特性，如可中断锁的获取（synchronized在等待获取所时时不可中断的），超时中断锁的获取，等待唤醒机制的多条件变量**Condition**等。

ReetrantLock可以支持一个线程对资源重复加锁，同时也支持公平锁与非公平锁，先对锁进行请求的就一定先获取到锁，那么这就是公平锁，反之，如果对于锁的获取并没有时间上的先后顺序，如后请求的线程可能先获取到锁，这就是非公平锁。非公平锁机制的效率往往会胜过公平锁的机制，但在某些场景下，可能更注重时间先后顺序，那么公平锁自然是很好的选择

AbstractQueuedSynchronizer队列同步器，内部通过state来控制同步状态，当state=0时，说明没有任何线程占有共享资源的锁，当state=1时，说明有线程正在使用共享变量，其他线程必须加入同步队列进行等待，AQS内部通过内部类Node构成FIFO的同步队列来完成线程获取锁的排队工作。同时利用内部类ConditionObject构建等待队列，当Condition调用wait()方法后，线程将会加入等待队列中。而当Condition调用signal()方法后，线程将从等待队列转移动同步队列中进行锁竞争，通过Condition调用await()方法释放锁后，将加入等待队列。

