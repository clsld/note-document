## socket编程

### 一、UDP和TCP

**TCP:** **tcp协议是一种面向连接、可靠、基于字节流的传输层通信协议，建立连接需要三次握手，第一次握手：客户端向服务器发送请求报文段，该报文段中包含自身的数据通信初始序号，请求发送后，客户端便进入SYN-SENT状态。第二次握手，服务端接受到客户端发送来的请求报文段后，如果同意连接，则会发送一个应答，该应答中包含自身的数据通信初始序号，发送完后进入SYN-RECOVERD状态。第三次握手，当客户端收到连接答应后，还要向服务端发送一个确认报文，客户端发完这个报文后便进入ESABLISHED状态，服务端接收到这个应答后进入ESTABLISHED状态，此时连接成功。**

**UDP:udp不需要和tcp一样传输数据前要进行三次握手建立连接，先发送数据就可以开始发送，并且只是数据的搬运工，不会对数据进行拆分或其他操作，不可靠性是说upd，想发就发，没有拥塞控制，所以不能确保说接收方能够接收到数据。**



### 二、使用封装函数获得IP信息

```c
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
int main(int argc, char **argv) {
	if(argc!=2){
		printf("usage : %s hostname\n",argv[0]);
		return -1;
	}//agrv[1] hostname

	struct hostent info;
	memset(&info,0,sizeof(struct hostent));
	info = gethostbyname(argv[1]);
	if(info == NULL){
		return -1;
	}
	pritnf("h_name : %s\n",info->h_name);
	int i;
	for(i=0;info->h_aliases[i] !=NULL;i++){
		printf("h_aliases %d:%s\n",i,info->h_aliases[i]);
	}
	if(info->h_addrtype == AF_INET){
		pritnf("addr type ipv4\n");
	}else{
		printf("addr type ipv6\n");
	}
	char ipstr[16];
	for(i=0;info->h_addr_list[i] !=NULL;i++){
		memset(ipstr,0,16);
		inet_ntop(info->h_addrtype,info->h_addr_list[i],ipstr,16);
			printf("h_addr_list %d:%s\n",i,info->h_addr_list[i]);
		}

	return 0;
}
```

