### yum

- 有时候输入yum install -y xxxx 出现 No package xxxx avaliable

  第一种解决办法是输入	sudo yum install epel-release 	if是安装docker的话可以看下面的解决方案。	造成这个现象的原因是yum没有找到xxxx包，更新epel第三方软件库就好

- Centos6 安装docker出现No package docker-io available

  该解决方案是针对安装docker不成功的情况，百度中给出大多数操作为下面两个步骤：

  yum install epel-release  更新epel第三方插件库	yum install docker-io

  不过还是报错 No package docker-io available

  j解决办法：cd /etc/yum.repos.d	sudo wget http://www.hop5.in/yum/el6/hop5.repo

  yum install docker-io	就OK了



