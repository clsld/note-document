# HashMap原理与实现

[TOC]



| DEFAULT_INITTAL_CAPACITY  | table数组的初始化长度:1<<4即2^4=16                           |
| :-----------------------: | ------------------------------------------------------------ |
|     MAXIMUM_CAPACITY      | table数组的最大长度：1<<30即2^30=1073741824                  |
|    DEFAULT_LOAD_FACTOR    | 负载因子：默认值为8,表示在一个node节点下的值大于8时并且数组的长度大于64，才会将链表转换为红黑树 |
|    UNTREEIFY_THRESHOLD    | 红黑树链化阈值：默认值为6，表示在进行扩容期间，单个node节点下的红黑树节点的个数小于6的时候，会将红黑树转化为链表 |
| MIN_TREEIFY_CAPACITY = 64 | 最小树化的阈值，当table所有元素超过64并且节点的数量超过8才会进行树化（为了防止前期阶段频繁扩容和树化过程冲突） |
|                           |                                                              |

## hash冲突

> 我们都知道在hashmap中使用数组加链表，数组使用起来是有下标的，但是我们平时使用的hashmap都是直接把值赋给key（例如hashmap.put(“aaa”,“zm”），我们并没有为我们存放进来的值指定下标或者说我们并不是用key作为数组的下标，而是对key值进行hash code()方法生成一个值，但是如果这个值很大的话，甚至超出数组容量的话，我们就把它和数组的长度进行取余方法（例如key.hashcode()%table.length）这样对于任意一个key值，我们都能为他找到相应的数组入口。

## hashMap的get/put过程

### put方法

> 1. 对于key的hashcode()做与运算，计算index
> 2. 如果没有碰撞就直接放在bucket里
> 3. 如果碰撞了就以链表的形式存在bucket后
> 4. 如果碰撞导致链表过长（大于等于TREEIFY_THRESHOLD），就把链表转换为红黑树
> 5. 如果节点已经存在就替换old value（保证key的一致性）
> 6. 如果bucket满了(超过load factor*current capacity)，就要resize

### get方法

> 1. 对于key的hashcode()做与运算，计算index
> 2. 如果在bucket里的第一个节点就直接命中则直接返回
> 3. 如果有冲突，则通过key.equals(k)去查找对应的Entry
> 4. 若为树，则在树中通过key.equals(k)查找，O(logn)
> 5. 若为链表，则在链表中通过key.equals(查找),O(n)

