# Spring面试题

[TOC]



## 概述

### 用到的设计模式

> 工厂模式：BeanFactory就是简单的工厂模式的体现，用来创建对象的实例
>
> 单例模式：Bean默认为单例模式
>
> 代理模式：Spring的AOP功能用到了JDK的动态代理和CGLB字节码生成技术
>
> 模板方法：用来解决代码重复的问题，比如RestTemplate、JmsTemplate,JpaTemplate
>
> 观察者模式：定义对象键一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都会得到通知被制动更新，如Spring中Listener的实现ApplicationListener

### 使用spring需要导入的jar

> - Spring-core:包含Spring框架基本的核心工具类，SPring的其他组件都要用这个包中的类，是其他组件的基本核心
> - Spring-beans：包含访问配置文件、创建和管理bean以及进行IOC或者DI操作的相关所有类
> - Spring-context：Spring提供在基础IOC功能上的扩展，还提供很对企业级服务的支持，如邮件服务、任务调度
> - Spring-expression:定义Spring的表达式语句
> - commons-logging：处理日志信息

## 控制反转（IOC）

### 什么是IOC?

IOC是指在程序开发过程中，对象实例的创建不再由调用者管理，而是由Spring容器创建，Spring容器会负责控制程序之间的关系，而不是由代码直接控制，因此控制权由程序代码转移到了Spring容器，控制权发生了反转，即控制反转。Spring IOC提供了两种容器，分别是BeanFactory和ApplicationContext。

### BeanFacory

BeanFactory是基础的IOC容器，是一个接口，提功了完整的IOC服务，BeanFactory是一个管理Bean的工厂，他主要负责初始化各种bean，并调用他们的生命周期方法。BeanFactory接口有多个实现类，最常见的是XmlBeanFactory，他根据Xml配置文件中定义装配Bean。下面是创建XmlBeanFactory对象传入xml文件的例子

```java
BeanFactory beanfactory = new XmlBeanFactory(newFileSystemResource("D://applicationContext.xml"));
```

### ApplicationContext

ApplicationContext是BeanFactory的子接口，也被称为应用上下文，不仅提供了BeanFactory的所有功能，还添加了对国际化、资源访问、事件传播等方面的支持。

ApplicationContext提供了两个实现类

- CLassPathXmlApplicationContext该类从类路径ClassPath中寻找指定的xml文件。
- FileSystemXmlApplicationContext该类从指定文件系统路径中寻找指定的xml文件

### BeanFactory和ApplicationContext的区别

如果Bean的某一个属性没有注入，若使用BeanFactory加载后，在第一次调用getBean方法时会抛出异常，而ApplicationContext会在初始化是检查，有利于检查所属依赖的属性是否注入。

### 依赖注入（DI）

> 依赖注入和控制反转含义相同，他们是同不同的角度描述同一个概念。当某个对象实例化时，传统的方法时由调用子创建被调用者的实例，比如使用new，而使用spring框架后，被调用者的实例不再由调用者创建，而是讲给了spring容器。spring容器在创建被调用实例时，会自动将调用者需要的对象实例注入给调用者，这样通过spring容器就活的了被调用者的实例，称为依赖注入。
>
> #### 依赖注入的实现
>
> - 属性setter注入
>
> ```java
> //创建类A
> public class A{
>     public A(){
>         System.out.println("正在初始化类A,调用无参构造器A");
>             public void print(){
>             System.out.println("调用了类A的print方法");
>         }
>     }
> }
> 
> //创建类B
> public class B{
>     private A a;
>     public void setA(A a){
>         this.a = a;
>     }
>     public void print(){
>         a.print();
>     }
> }
> 
> //配置Beans.xml
> <bean id="a" class="com.clsld.ioc.A"/>
> <bean id="b" class="com.clsld.ioc.B">
>     <property name="a" ref="a"/>
> </bean>
> //测试
> @Test
>     public void test(){
> 		B b = (B) context.getBean("b");
>     	b.print();
> 	}
> }
> ```
>
> - 构造器注入
>
> ```java
> //配置Beans.xml
>     <bean id="a" class="com.ssm.ioc.A"/>
>     <bean id="b" class="com.ssm.ioc.B">
>         <constructor-arg name="a" ref="a"/>
>     </bean>
> //测试
>     @Test
>     public void test() {
>         B b = (B) context.getBean("b");
>         b.print();
>     }
> ```
>
> - 静态工厂方式实例化
>
> ```java
> //创建工厂类
> public class MyBeanFactory {
>     public static A createCoffer() {
>         return new A();
>     }
> }
> //配置Beans.xml
> <bean id="a" class="com.ssm.ioc.MyBeanFactory" factory-method="createA"/>
> //测试
>     @Test
>     public void test() {
>         A a = (A) context.getBean("a");
>     }
> ```
>
> - 实例工厂实例化
>
> ```java
> //创建工厂类
> public class MyBeanFactory {
>     public MyBeanFactory() {
>         System.out.println("A工厂实例化中。。。");
>     }
>     public A createBean() {
>         return new A();
>     }
> }
> //配置Beans.xml
>     <bean id="myBeanFactory" class="com.ssm.ioc.MyBeanFactory"/>
>     <bean id="a" factory-bean="myBeanFactory" factory-method="createBean"/>
> //测试
>     @Test
>     public void test() {
>         A a = (A) context.getBean("a");
>     }
> ```

## SpringBean

## 注解

## 面向切面编程

