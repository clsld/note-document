# volatile的原理与作用



[TOC]

## 内存模型

### 计算机内存模型

现代计算机的cpu的指令速度远超内存的读取速度，因此现代计算机系统不得不在内存和处理之间加入一层读写速度尽可能接近处理器运算速度的高速缓存来作为缓冲。将运算需要用到的数据复制到高速缓存中，能让运算更加快速的进行，当结束运算后在将缓存同步会内存中。在多cpu的处理器系统中，每个处理器都有自己的高速缓存，而它们又共享着同一块主内存。

当要求使用volatile关键字的时候，系统会重新从他所在的内存中读取数据，即时它前面的指令刚刚从主内存中读取过数据，准确的说就是遇到这个关键字声明的变量，编译器对访问该变量的代码就不再进行优化，从而可以提供对特殊地址的稳定访问，如果不使用volatile则编译器会将对没有volatile声明的语句进行优化。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/uChmeeX1FpzhiaXUhn9W2XjuqeziaG1ibdvVI870whHRM2h2vfaCl1RRIXLpia50cmaSrQZ96z2Z2fMDOUn3gRwDXA/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### JMM(java内存模型)

#### 概念

描述java程序中各种线程共享变量的访问规制，以及在jvm中将变量存储到内存和从内存中读取变量的底层细节。

#### JMM规定

1. 所有的共享变量都存储于主内存，这里说的变量指的是实例变量和类变量，不包括局部变量，局部变量是线程私有的，不存在不同线程的访问问题。
2. 每一个线程还有自己的工作内存，在工作内存中保留着被线程使用的变量的工作副本。线程对变量所有的读写操作都必须在工作内存中完成的，而不能直接读写内存中的变量。
3. 不同线程之间不能直接访问对方工作内存中的变量，线程间变量的值的传递需要通过主内存中转来完成。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/uChmeeX1FpzhiaXUhn9W2XjuqeziaG1ibdvOgPyiaPib3U7oR6ZS77CqlAVp7BkTxS30UhDN1X6YJRfCGQadBP6xd9Q/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

## volatile的作用

## volatile的作用

- 防止指令重排序
- 保证内存可见

### 保证内存可见性

#### 加锁

加锁也可以保证内存的可见性，加上synchronized关键字的代码块，线程会获得锁，清空工作内存，从主内存拷贝共享变量最新的值到工作内存成为副本，执行代码，将修改后的副本的值刷新回主内存中，线程释放锁。而获取不到锁的线程会阻塞，所以变量的值一直都是最新的。

### volatile

volatile保证不同线程对共享变量操作的可见性，也就是说一个线程修改了volatile修饰的变量，当修改写回主内存时，另外一个线程立即会看到最新的值。

### 原子性

java只有对基本类型变量的赋值和读取是原子操作，如i = 1的赋值操作，但是像j = i货值 i++这样的操作都不是原子操作，因为他们进行了多次的原子操作，是先读取i的值再将i的值赋值给j，两个原子操作加起来就不是原子操作了。举个例子，比如有两个线程A和B，共享变量i被volatile修饰，其中A读取i的值为100，就在此时还没进行还没来得及修改值，线程B就读取了i的值为100，随后对i进行自增变为101，当修改的值还没有同步到主内存时线程A写操作操作的值还是之前读取到的100，所以volatile不能保证原子性。

