### nodejs-http模板

**打node-http-server可导入下面的HTTP模板**

```javascript
//引入http模块
var http = require('http');
//创建web服务
//request获取url传来的信息,response给浏览器返回的信息
http.createServer(function (request, response) {
    //设置相应头
    response.writeHead(200, {'Content-Type': 'text/plain'});
    //表示我们在页面上输出一句话后结束
    response.end('Hello World');
    //开启的端口监听
}).listen(8081);
console.log('Server running at http://127.0.0.1:8081/');
```



### nodejs-解析URL地址

```javascript
const http = require('http');
const url = require('url');

http.createServer((req,res)=>{
    res.writeHead(200,{"Content-type":"text/html;charset='utf-8'"});//解决乱码问题
    res.write("<head><meta charset='UTF-8'></head>");//解决问题
    console.log(req.url);
    if(req.url!='/favicon.ico'){
        var userinfo = url.parse(req.url,true).query;
        //注意：使用模板取值的话不能用单双引号，要用tab键上面的反引号
        console.log(`姓名：${userinfo.name}--年龄：${userinfo.age}`);
    }
    res.end();
}).listen(8001);
```



### nodejs-导入模块化

#### **md5加密和时间格式**

> ```javascript
> //npm install md5 --save
> //下载md5加密模块，save表示写入dependces的json中，在以后可以npm i下载删除了的packages
> const md5 = require('md5');
> //下载日期格式化模块，如果模块后面加 @1.1，即silly-datetime@1.1 是下载指定版本
> const sillyDateTime = require('silly-datetime');
> //对123456进行加密
> console.log(md5('123456'));
> //对当前时间进行自定义格式化输出
> var t = sillyDateTime.format(new Date(),'YYYY-MM-DD HH:mm:ss');
> //对时间使用默认格式化输出
> console.log(sillyDateTime(new Date()));
> console.log(t);
> ```

#### fs文件操作模块

> **1、fs.stat 	检测是文件还是目录**
>
> **2、fs.mkdir	创建目录**
>
> **3、fs.writeFire	创建写入文件**
>
> **4、fs.appendFile	追加文件**
>
> **5、fs.readFile	读取文件**
>
> **6、fs.readdir	读取目录**
>
> **7、fs.rename	重命名**
>
> **8、fs.rmdir	删除目录**
>
> **9、fs.unlink	删除文件**



```javascript
//判断服务器上main有没有upload目录。如果没有就创建这个目录，如果有的话就不创建
//fs.stat fs.mkdir
const fs = require('fs');
fs.stat('./upload',(err,data)=>{
    if(err){
    //折行创建目录
        console.log("不存在文件和目录！")
        fs.mkdir('./upload',(err)=>{
            if(err){
                console.log("创建目录失败！")
            }
        })
        return 0;
    }
    if(data.isDirectory){
        console.log("已存在该目录！");
        return 0;
    }
    if(data.isFile){
        console.log("存在文件，不存在目录！")
        fs.mkdir('./upload',(err)=>{
            if(err){
                console.log("创建目录失败！")
            }
        })
    }
})
```





### nodejs-异步操作

```javascript
// //问题：异步操作获取不到function内容的数据
// function getData(){
//     setTimeout(()=>{
//         var name = "张三";
//     },1000);
//     return name;
// }
// //外部获取不到异步方法里面的数据
// console.log(getData());


//解决办法1：通过回调函数获取异步方法里的数据
// 解析：
// getData01(callback){}里面的
// callback运用
// getData01((name)=>{
//     console.log(name);
// });代替,所以callback就变为下面的
// var callback=function(name){
//     console.log(name);
// }又因为本身函数又调用了callback函数，所以原先的数据就给形参callback赋值，
// 即把name=张三赋值给name=null,并调用形参的方法console出来
// callback(name)
function getData01(callback){
    setTimeout(()=>{
        var name = "张三";
        callback(name)
    },1000);
}
getData01((name)=>{
    console.log(name);
});


//解决办法二：通过promise来处理异步
//resolve:成功的回调    reject：失败的回调
// 通过then获取回调成功的值
//写法一
var p = new Promise((resolve,reject)=>{
    setTimeout(()=>{
        var name = "张三1";
        resolve(name);
    },1000);
})
p.then((data)=>{
    console.log(data);
});
// 写法二：即封装
var p1 = new Promise(getData02);
function getData02(resolve,reject){
    setTimeout(() => {
        var name =  "张三2";
        resolve(name);
    }, 1000);
}
p1.then((data)=>{
    console.log(data);
})


//1.async是申明一个异步function的简写，即在方法前面加上async的话这个方法就变成异步方法
//  1-1
async function a(){
    return 'hello async!'
}
console.log(a());
//折行程序返回一个promise对象：Promise{'hello async!'}
//  1-2
async function b(){
    return 'hello async!'
}
async function b1(){
    //await必须在async中才能使用
    var word = await b();
    console.log(word);
}
b1();

```

```javascript
// 练习：wwwroot文件夹下面有images css js 以及index.html ，
// 找出wwwroot目录下的所有目录，并把他们添加到一个数组中
//方法一：用递归的办法躲避异步的问题
const fs = require('fs');
var path = './wwwroot';
var dirArr=[];
fs.readdir(path,(err,data)=>{
    if (err){

        console.log(err);
        return;
    }
        (function getDir(i){
            if(i == data.length){
                console.log(dirArr);
                return;
            }
            fs.stat(path+'/'+data[i],(error,stats)=>{
                if(stats.isDirectory()){
                    dirArr.push(data[i]);
                }
                getDir(i+1);
            })
        })(0);
})

//定义一个方法来判断这个资源是文件还是目录
async function isDir(path){
   return new Promise((resolve,rejrct)=>{
       fs.stat(path,(error,stats)=>{
           if(error){
                console.log(error);
                rejects(error);
                return;
           }
           if(stats.isDirectory){
                resolve(true); 
           }
           else{
               resolve(false);
           }
       })
   })
}

// 获取wwwroot的资源，循环遍历
function main(){
    var path = './wwwroot';
    var dirArr = [];
    fs.readdir(path,async(err,data)=>{
        if(err){
            console.log(err);
            return
        }
        for(var i=0;i<data.length;i++){
            if(await isDir(path+'/'+data[i])){
                dirArr.push(data[i]);
            }
        }
        console.log(dirArr);
    })
}
main();
```

### nodejs-express剖析

```javascript
/*
最终目标是以这样的方式配置路由
app.get("/",function(req,res){
    res.send('hello world');
})
*/
let app = function(){
    console.log('app方法!');
}
app.get = function(){
    console.log('get方法!');
}
app.post = function(){
    console.log('post方式!');
}

//框架剖析精华
let G={};

let app = function(){
    if(G['/login']){
        G['/login']();
    }
}	
app.get = function(string,func){
    G[string] = func;
/*
    G['/login'] = function(req,res){
        res.send('hello world')
    }
*/
}
```

### nodejs-连接mongodb

```javascript
const mongodb = require('mongodb').MongoClient;
const assert = require('assert');
const { MongoClient } = require('mongodb');
const { callbackify } = require('util');

// connection URL
const url = 'mongodb://localhost:27017';

//database Name
const dbName = 'itying'

//creat a new mongoClient
const client = new MongoClient(url);

//use connect method to connect to the server
client.connect((err)=>{
    assert.equal(null,err);
    console.log("connected successfully to server");
    const db = client.db(dbName);
    const collection = db.collection('user');
    collection.insertMany([
        {a:1},{b:2},{c:3}
    ],(err,result)=>{
        assert.equal(err,null);
        assert.equal(3,result.result.n);
        assert.equal(3,result.ops.length);
        console.log("Inserted 3 documents into the collection")
    })
    client.close();
});

client.connect((err)=>{
    assert.equal(null,err);
    console.log("connected successfully to server");
    const db = client.db(dbName);
    const collection = db.collection('user',{useUnifiedTopology: true });
    collection.find({"age":20}).toArray((err,data)=>{
        console.log(data);
        client.close();
})
```

### nodejs-expree框架

#### 获得url参数

```javascript
//获取浏览器get命令后的参数
//localhost:3000/?id=111&password=123w1e
//获取id和password
const express = require('express')
const app = express()//实例化
const port = 3000//端口号建议写成3000以上
app.get('/', (req, res) =>{
    let query = req.query
    console.log(query);
    res.send(query.id)
    })
// 监听端口
app.listen(port)
```

#### ejs模板引擎

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2><%=title%></h2>
</body>
</html>
//还文件的名称为index.ejs
```

```javascript
const express = require('express')
const app = express()
const port = 3000
app.set("view engine","ejs")

app.get('/', (req, res) =>{
    var title = "hello world";
    res.render("index",{
        title:title
    })
})
// 监听端口
app.listen(port)
```

#### nodejs-获取和设置cookie

```javascript
const express = require('express');
const cookie = require('cookie-parser');
var app = express();
app.use(cookie());

app.get('/a',(req,res)=>{
    res.cookie("usernanme","zhangshan",{maxAge:1000*60*60});
    console.log("cookie:",req.cookies);
    res.send("hello world!")
})

app.listen(3000);
```

#### nodejs-获取和设置session

```javascript
const express = require('express')
const session = require('express-session')
const app =express();
// 配置session中间件
app.use(
    session({
    secret:'keyboard cat',//服务端生成申明可随意写
    resave:true,//强制保存session即使他没有什么变化
    saveUninitialized:true,//强制将来初始化的session存储
    cookie:{//session是基于cookie的，所以可以在配置session的时候配置cookie|
        maxAge:1000*60,//设置过期时间
        secure:false//true的话表示只有https协议才能访问cookie
    }
}))

app.get("/put",(req,res)=>{
    req.session.username = "zhangshan"
    res.send("设置cookie成功！")
})

app.get("/get",(req,res)=>{
    console.log(req.session.username)
    res.send("获取cookie成功！");    
})

app.listen(3000)

// 销毁session的三种方法 
    //第一种： 设置session的过期时间为0（他会把所有的session都销毁）
    req.session.cookie.maxAge = 0
    //第二种:  销毁指定的session
    req.session.username = ""
    //第三种:  销毁session的destory
    req.session.destroy();
```



### nodejs-路由挂载

**路由模块**

```javascript
const express = require('express')

const router = express.Router()

router.get('/',(req,res)=>{
    res.send("显示用户登录")
})

router.get('/dologin',(req,res)=>{
    res.send("折行登录")
})

module.exports = router
```

**折行挂载模块**

```javascript
const express = require('express')

const app = express()
//引入外部模块
const login = require('./routes/login')

app.use('/',login)

app.listen(8080);	
```

### nodejs-express模块化编程

#### 项目结构



![image-20200725212130527](C:\Users\clsld\AppData\Roaming\Typora\typora-user-images\image-20200725212130527.png)

​															**（其中pakages.json没有截入其中）**

#### 项目详细案例代码

##### app.js

```javascript
const express = require('express')
const bodyParser = require('body-parser')
const ejs = require('ejs')
const app = express()
// 引入外部模块
const admin = require('./admin')
const index = require('./index')
const api = require('./api')
//配置模板引擎
app.engine("html",ejs.__express)
app.set("view engine","html")
// 配置静态web目录
app.use(express.static("static"))
// 配置第三方中间件
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
//使用路由
app.use('/index',index)
app.use('/api',api)
app.use('/admin',admin)
// 接口
app.get('/',(req,res)=>{
    res.send("/")
})
app.listen(8080);
```

##### index.js

```javascript
const express = require('express')

var router = express.Router()

router.get('/',(req,res)=>{
    res.send("首页")
})

module.exports = router
```

##### api.js

```javascript
const express = require('express')

var router = express.Router()

router.get('/',(req,res)=>{
    res.send("api接口")
})
module.exports = router
```

##### admin.js(和子路由模块)

> login.js
>
> ```javascript
> const express = require('express')
> 
> var router = express.Router()
> 
> router.get('/',(req,res)=>{
>     res.send("用户登录页面")
> })
> 
> router.post('/dologin',(req,res)=>{
>     res.send("折行登录")
> })
> 
> 
> module.exports = router
> ```
>
> nav.js
>
> ```javascript
> const express = require('express')
> 
> var router = express.Router()
> 
> router.get('/',(req,res)=>{
>     res.send("导航列表")
> })
> 
> router.get('/add',(req,res)=>{
>     res.send("增加导航")
> })
> 
> router.get('/adit',(req,res)=>{
>     res.send("修改导航")
> })
> 
> router.post('/doadd',(req,res)=>{
>     res.send("折行增加")
> })
> 
> router.post('/doedit',(req,res)=>{
>     res.send("折行修改")
> })
> 
> module.exports = router
> ```
>
> user.js
>
> ```javascript
> const express = require('express')
> 
> var router = express.Router()
> 
> router.get('/',(req,res)=>{
>     res.send("用户列表")
> })
> 
> router.get('/add',(req,res)=>{
>     res.send("增加用户")
> })
> 
> router.get('/adit',(req,res)=>{
>     res.send("修改用户")
> })
> 
> router.post('/doadd',(req,res)=>{
>     res.send("折行增加")
> })
> 
> router.post('/doedit',(req,res)=>{
>     res.send("折行修改")
> })
> 
> module.exports = router
> ```
>
> 

### nodejs-express生成模板

#### 下载生成模板所需要的包

![image-20200725214757831](C:\Users\clsld\AppData\Roaming\Typora\typora-user-images\image-20200725214757831.png)

#### 命令行输入如下命令生成模板

![image-20200725214655052](C:\Users\clsld\AppData\Roaming\Typora\typora-user-images\image-20200725214655052.png)

### nodejs-使用render模块

#### 文件目录

![image-20200726113441691](C:\Users\clsld\AppData\Roaming\Typora\typora-user-images\image-20200726113441691.png)

#### 新建add.html（此文件在views文件夹的目录下）

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>增加导航</title>
</head>
<body>
    <form action="/admin/nav/doadd" method="POST">//绑定一个post请求提交
    <input type="text" name="title" id="title">
    <br>
    <br>
    <textarea name="desc" id="desc" cols="30" rows="10"></textarea>
    <br>
    <br>
    <input type="submit" value="提交">
</form>	
</body>
</html>
```

#### 在nav.js中使用render绑定add.html

```javascript
const express = require('express')

var router = express.Router()

router.get('/',(req,res)=>{
    res.send("导航列表")
})

router.get('/add',(req,res)=>{
    //绑定add.html
    res.render("admin/nav/add")
})

router.get('/adit',(req,res)=>{
    res.send("修改导航")
})

router.post('/doadd',(req,res)=>{
//这是在add.html中action的接口
//通过body-parser获取post接口中的值
    var body = req.body;
    res.send(body)
    res.send("折行增加")
})

router.post('/doedit',(req,res)=>{
    res.send("折行修改")
})

module.exports = router
```

### nodejs-通过muter上传图片

#### 下载模块命令

```commonlisp
cnpm install multer --save
```

```javascript
const express = require('express')
//导入模块
const multer = require('multer')
//设置文件存放目录
const upload = multer({
    dest:'static/uploads/'
})
var router = express.Router()

router.get('/',(req,res)=>{
    res.send("导航列表")
})

router.get('/add',(req,res)=>{
    res.render("admin/nav/add")
})

router.get('/adit',(req,res)=>{
    res.send("修改导航")
})
//upload.single()绑定要传入的文件id
router.post('/doadd',upload.single("pic"),(req,res)=>{
    res.send({
        body:req.body,
        file:req.file
    })
})

router.post('/doedit',(req,res)=>{
    res.send("折行修改")
})

module.exports = router
```

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>增加导航</title>
</head>
<body>
    //enctype="multipart/from-data"启用传输文件设置
    <form action="/admin/nav/doadd" method="POST" enctype="multipart/form-data">
    <input type="text" name="title" id="title">
    <br>
    <br>
    //传输文件按钮
    <input type="file" name="pic" id="pic">
    <br>
    <br>
    <textarea name="desc" id="desc" cols="30" rows="10"></textarea>
    <br>
    <br>
    <input type="submit" value="提交">
</form>
</body>
</html>
```

### nodejs-mongoose操作数据库

```javascript
const mongoose  = require('mongoose')
mongoose.connect("mongodb://localhost:27017/itying")
//如果有设置用户和密码则采用下面的模板
// mongoose.connect("mongodb://clsld:123456@localhost:27017/itying")

/*定义schema
  数据库中的schema是数据库对象的集合。schema是mongoose中会使用的一种
 数据模式，可以理解为mongoose是通过schema来定义表结构的。每个schema会映射到
 MongoDB中的一个collection，它不具备有操作数据库增删改查的能力！
*/
//名字可以随便起，但是比如我们要操作use表就把schema函数名字定义为use
var UserSchema = mongoose.Schema({
    //schema里面的参数要和数据表的参数对应起来
    username:String,
    age:Number
})

/*定义数据库model,操作数据库
    定义好schema后，接下来就是生成model，model是由schema生成的模型，
    可以对数据库进行增删改查的操作！
*/
//这里的第一个参数对应集合的名称并且参数首字母要大写（即集合首字母是小写的话也要转化为大写才能生效）
//教程上是这样说的，不过自己试了一下发现如果是要找具体集合的话，即不找负数集合的话这里的第一个参数首字母可以小写
//这里的第二个参数对应定义的schema
//这里的第三个参数如果不写的话，model会找集合名称+s的那个集合，写了的话就是明确要找那个集合
var User = mongoose.model('User',UserSchema,'user')

/*查询use表的数据*/
User.find({},(err,doc)=>{
    if(err){
        console.log(err)
        return
    }
    console.log(doc)
})
```

