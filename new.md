kafka

顺序写入磁盘、read-ahead、write-behind、Memory Mapped Files

大量使用内存页

零拷贝技术

​	

### zookeeper

zookeeper的理解

应用场景

wather监听机制

如何保证数据一致性

怎么进行leader选举

如何进行数据同步

怎么解决数据不一致问题



### Elasticsearch

集群中可以包含多个==索引(indices)（数据库）==，

每一个索引可以包含多个==类型(types)（表）==，

每一个类型包含多个==文档(documents)（行）==，

然后每个文档包含多个==字段(Fields)（列）==。



CPA是什么？

一致性：当一个进程修改了数据之后，其他进程读取的数据要求是更新过的、

- 强一致性：更新操作后，任何进程都返回最新的值
- 弱一致性：系统不能保证返回最新值
- 最终一致性：系统保证没有后续更新的前提下，系统最终返回上次更新操作的值

可用性：用户发出请求，需要实时响应，不能等待其他事情完成后再响应、

容错性：一个系统由多个节点组成，部分节点坏了，整个系统能保证正常通信

CPA理论表明，在一个系统中，cap不可能同时满足，只能最多满足同时两条。



kafka保持消费顺序性、多个position的情况下

kafka要保证消费的顺序可以有两种方法：

1. 1个topic主题只创建一个partition分区，这样生产者的所有数据都会发送到一个partition分区，从而保证了消息的顺序性消费
2. 生产者在发送消息的时候指定要发送到哪个partition分区

联合索引和主键索引的区别（数据结构和使用上等）

怎么实现一个延迟队列、怎么做到队列阻塞的效果

怎么使用kafka或者java实现一个延迟阻塞队列



实现延时队列的方法

FIFO、LRU、表盘（Clock）和 LFU 淘汰算法

#### ARIES 数据恢复算法

这个算法全称为 Algorithm for Recovery and Isolation Exploiting Semantics。

该算法同时使用 undo log 和 redo log 来完成数据库故障崩溃后的恢复工作，其处理流程分为如下三个步骤。

1. 首先数据库重新启动后，进入分析模式。检查崩溃时数据库的脏页情况，用来识别需要从 redo 的什么位置开始恢复数据。同时搜集 undo 的信息去回滚未完成的事务。
2. 进入执行 redo 的阶段。该过程通过 redo log 的回放，将在页缓存中但是没有持久化到磁盘的数据恢复出来。这里注意，**除了恢复了已提交的数据，一部分未提交的数据也恢复出来了**。
3. 进入执行 undo 的阶段。这个阶段会回滚所有在上一阶段被恢复的未提交事务。为了防止该阶段执行时数据库再次崩溃，存储引擎会记录下已执行的 undo 操作，防止它们重复被执行。

读写异常：

脏读、不可重复读、幻读；丢失更新、脏写、写偏序

LSM树



avl树和红黑树的区别

treemap和treeSet

linkedhashmap和likehashset

hashmap和hashset

map和set

weakhashmap



### JDK动态代理

动态代理的核心是InvocationHandler接口，下面有一个InvocationHandler的demo实现

```java
public class DemoInvokerHandler implements Invocation InvocationHandler{
    private Object target;
    public DemoInvokerHandler(Object target){
        this.target = target;
    }
    public Object invoke(Object proxy,Method method,Object[] args) throws Throwsable{
        //方法预处理
        Object result = method.invoke(target,args);
        //方法后置处理
       return result;
    }
    public Object getProxy(){
        //创建代理对象
        return Proxy.newProxyInstance(Thread.currentThread()
                                      .getContextClassLoader(),
                                      target.getClass().getInterfaces(),this);
    }
}
```

接下来创建一个main()方法来模拟调用上层的调用者，创建并使用动态代理

```java
public class Main{
    public static void main(String[] args){
        Subject subject = new RealSubject();
        DemoInvokerHandler invokerHandler = new DemoInvokerHandler(Subject);
        //获取代理对象
        Subject proxy = (Subject)invoketHandler.getProxy();
        //调用代理对象的方法，它会调用DemoInvokerHandler.invoke()方法
        proxy.operation();
    }
}
```



了解jdk的动态代理基本使用后，下面我们来分析jdk动态代理创建代理类的底层实现原理

jdk动态代理相关实现的入口是Proxy.newProxyInstance()这个静态方法，它的三个参数分别是加载动态生成的代理类的类加载器、业务类实现的接口和上面介绍的InvocationHandler对象。Proxy.newProxyInstance()方法的具体实现如下:

```java
public static Object newProxyInstance(ClassLoader loader,Class[] interfaces.InvocationHandler h) throws IllegalArgumentException{
	final Class<?>[] intfs = interfaces.clone();
    //省略权限检查的代码
    Class<?> cl = gwtProxyClass0(loader,intfs);//获取代理类
    //省略try、catch代码块和相关异常处理逻辑
    final Constructor<?> cons = cl.getConstructor(constructorParams);
    final InvocationHandler ih = h;
    return cons.newInstance(new Object[]{h});//创建代理对象
}
```

通过newProxyInstance()方法的实现可以看到，jdk代理是在getProxyClass0()方法中完成完成代理类的生成和加载。getProxyClass0()方法的具体实现如下：

```java
private static class getProxyClass0(ClassLoader loader,Class... interfaces){
    //边界检查，限制接口数量
    //如果指定的类加载器中已经创建了实现指定接口的代理类，则查找缓存
    return proxyClassCache.get(loader,interfaces);
}
```

ProxyClassCache是定义在Proxy类中的静态字段，主要用于缓存已经创建过的代理类，定义如下：

```java
private static final WeakCache[],Class> proxyClassCache = new WeakCache<>(new KeyFactory,new ProxyClassFactory());
```



1、排序算法的复习和DFS、BFS的过程

插入排序、快速排序、归并排序、选择排序

2、mysql的复习



3、零拷贝和虚拟内存

4、redis的复习




SpringBoot的依赖注入、循环依赖的解决

A->B->C 标识线程上下文的传递机制

redis的底层数据结构、Mysql的触发器机制、执行器、bufferpool机制、刷脏



Kafka的机制、zookeeper的执行机制

线程的状态、object.wait()和Thread.sleep()分别对线程的作用

select、epoll、poll以及NIO、BIO、AIO、reactor的模型及netty的一些面试题

hashmap的get和put过程

next key lock



跳台阶、十进制转二进制与二进制转十进制、三门问题





