## JDBC基础

### 一、template

####  1、注册

```java
Class.forName(“sun.jdbc.JdbcOdbcDriver”);
```

#### 2、指定一个数据源（打开一个数据库连接）

```java
String url=“jdbc:odbc:mydata”;
	String username=“sa”;
	String password=“sa”;
	Connection conn=DriverManager.getConnection(url,username,password);
```

#### 3.利用Connection对象创建Statement对象(createStatement()，执行SQL命令)

```java
Statement stat=conn.createStatement();
	String sql_String=“update course set cname=‘Java Web’ where cid=‘01’”;
	stat.executeUpdate(sql_String);//折行sql
```

#### 4.若是查询返回一个结果集合

```java
ResultSet rs=stat.executeQuery(“select cid,cname from course”);
while(rs.next()){//利用next方法可从结果集中获取每一行记录，直至为false
	System.out.print(“rs.getString(“cid”)”+”\t”);// System.out.print(“rs.getString(1)”+”\t”);
    System.out.print(“rs.getString(“cname”)”+”\t”);// System.out.print(“rs.getString(2)”+”\t”);
    System.out.println();
}
```

#### 5.关闭连接

```java
rs.close()
stat.close()
conn.close()
```

### 二、little tip

> ⑴每个Connection对象可以创建一个或以上的Statement对象；
>
> ⑵同一个Statement对象可以用于多个不相关的命令和查询；
>
> ⑶一个Statement对象最多只能打开一个结果集，若想得到多个查询的结果集，创建多个Statement对象；
>
> ⑷Sql Server JDBC驱动程序只允许同时存在一个激活的Statement对象。
>
> (5)使用 Class.forName() 方法可以将驱动程序加载到 Java 解释器中
>
> (6)使用 DriverManager 类的 getConnection() 方法和 Connection 对象的 createStatement() 方法可建立连接
>
> (7)最后，使用 executeQuery() 或 executeUpdate() 方法通过 Statement 实例构建并执行 SQL 语句
>
> (8)PreparedStatement 接口允许创建预编译的 SQL 语句，并使得在后续阶段可以指定语句的参数
>
> (9)结果集可以是可滚动的，也可以是不可滚动的

### 三、test

