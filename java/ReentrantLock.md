# ReentrantLock



*java中的大部分同步类（Lock、Semaphore、ReentrantLock等）都是基于AbstractQueuedSynchronizer（AQS）实现的。AQS是提供一种**原子式管理同步状态**、**阻塞**和**唤醒线程功能**以及**队列模型**的简单框架*



## ReentrantLock特性概述

ReentrantLock的意思就是可重入锁，指的是一个线程能够对一个临界资源重复加锁。

和synchronized的比较如下：

|            | ReentranrLock                  | Synchronized      |
| ---------- | ------------------------------ | ----------------- |
| 锁实现机制 | AQS                            | Monitor监视器模式 |
| 灵活性     | 支持响应中断、超时、尝试获取锁 | 不灵活            |
| 锁释放形式 | 必须显示调用unlock()释放锁     | 自动释放monitor   |
| 锁类型     | 公平锁&非公平锁                | 非公平锁          |
| 条件队列   | 可关联多个队列条件             | 关联一个队列条件  |
| 可重入性   | 可重入                         | 可重入            |

直接用伪代码展示使用方法

```java
//用于代码块
synchronized(this){}
//用于对象
synchronized(object){}
//用于方法
public synchronized void test(){}
//可重入
for(int i =0 ;i < 100 ; i++){
    synchronized(this){}
}

public void test() throw Exception{
    //初始化选择公平锁和非公平锁
    ReentrantLock lock = new ReentrantLock(true);
    //可用于代码块
    lock.lock();
    try{
        try{
            //支持多种加锁方式，比较灵活，具有可重入性
            if(lock.tryLock(100,Timeout.MILLTSECOND)){}
        }finally{
            //手动释放锁
            lock.unlock();
        }
    }finally{
        lock.unlock():
    }
}
```

## ReentrantLock与AQS的关联

非公平锁的加锁流程如下：

```java
static final class NonfairSync extends Sync{
    ...
    final void lock(){
        if(compareAndSetState(0,1))
            setExclusiveOwnerThread(Thread.currentThread());
        else
            acquire(1);
    }
    ...
}
```

这块代码块的含义为：

- 若CAS设置变量State成功，也就是获取锁成功，则将当前线程设置为独占线程
- 若CAS设置变量State失败，也就是获取锁失败，则进入Acquire方法进行后续处理

那么在这个或许处理其实可以存在以下两种方案

1. 将当前线程获取锁结果设置为失败，获取锁的流程结束，这种设计会极大降低系统的并发度，并不能满足我们的实际需求
2. 存在某种队列等候机制，线程继续等待，仍然保留获取锁的可能

那么围绕着第二种方案有产生三个问题

1. 既然说到了排队等候机制，那么就一定会有某种队列形成，那这样的队列是什么数据结构呢？
2. 处于排队等候机制中的线程什么时候可以有机会获取锁？
3. 如果是处于排队等候机制中的线程一直无法获取锁，还需要一直等待吗，有没有什么策略来解决这一问题？



公平锁的加锁流程如下：

```java
static final class FairSync extends Sync{
    ...
    final void lock(){
        acquire(1);
    }    
    ...
}
```

lock函数通过acquire方法进行加锁，那具体是如何进行加锁的呢？

结合公平锁和非公平锁的加锁流程，虽然流程上有一定的不同，但是都调用了acquire方法，而acquire方法是FairSync和UnfairSync的父类AQS中的核心方法，所以以上的很多问题其实在ReentrantLock类的源码中都无法解答，而能解答这些问题的入口就在AbstractQueuedSynchronizer。

## AQS原理概述

### AQS的框架

![img](https://p1.meituan.net/travelcube/82077ccf14127a87b77cefd1ccf562d3253591.png)

总的来说，AQS框架分为5层，自上而下由浅入深，从AQS对外暴露的API到底层基础数据

当有自定义同步器接入是，只需要重写第一层所需要的部分方法即可，不需要关注底层具体的实现流程。当自定义同步器进行加锁或者解锁操作时，先经过第一层的api进入AQS内部方法，然后经过第二层进行锁的获取，接着对于获取锁失败的流程，进入第三层和第四层等待队列处理，而这些处理方式均依赖于第五层的底层数据提供层。

==AQS的核心思想是，如果被请求的共享资源空闲，那么就将当前请求资源的线程设置为有效的工作线程，将共享资源设置为锁定状态；如果共享资源被占用，就需要一定的阻塞等待唤醒机制来保证锁分配。这个机制主要用的是CLH队列的变体实现的，将暂时获取不到锁的线程加入到队列中。==

AQS队列是CLH队列变体的虚拟双向队列（FIFO）,AQS是通过将每条请求共享资源的线程封装成一个节点来实现锁的分配。

主要原理如下：

![img](https://p0.meituan.net/travelcube/7132e4cef44c26f62835b197b239147b18062.png)

AQS通过使用volatile声明一个int类型的state变量来表示同步状态，通过内置的FIFO队列来完成资源获取的排队工作，通过CAS完成对state值的修改。

```java
private volatile int state;
```

并且提供几个访问该字段的方法：

| 方法名                                                       | 描述                 |
| ------------------------------------------------------------ | -------------------- |
| protected final int getState()                               | 获取State的值        |
| protected final void setState(int newState)                  | 设置State的值        |
| protected final boolean compareAndSetState(int expect,int update) | 使用CAS方式更新State |

这几个方法都是final修饰的，说明子类中无法重写他们，我们可以通过修改State字段表示的同步状态来实现多线程的独占模式和共享模式的加锁过程

AQS的重要方法与ReentrantLock的关联

AQS提供了大量用于自定义同步器实现的protected方法，自定义同步器实现的相关方法知识为了通过修改state字段来实现多线程的独占模式或者共享模式，自定义同步器需要实现一下方法，一般来说，自定义同步器要么是独占方式，要么是共享方式，他们只需要实现tryAcquire-tryRelease、tryAcquireShared-tryReleaseShared中的一种即可。

| 方法名                                      | 描述                                                         |
| ------------------------------------------- | ------------------------------------------------------------ |
| protected boolean isHeldExclusively()       | 该线程是否正在独占资源，只有用到Condition才需要去实现它      |
| protected boolean tryAcquire(int arg)       | 独占方式，arg为获取锁的次数，尝试获取资源，成功则返回true，失败则返回false |
| protected boolean tryRelease(int arg)       | 独占方式，arg为释放锁的次数，尝试释放资源，成功则返回true，失败则返回false |
| protected int tryAcquireShared(int arg)     | 共享方式，arg为获取锁的次数，尝试获取资源，负数表示失败；0表示成功，但没有剩余可用资源；正数表示成功，且有剩余资源 |
| protected boolean tryReleaseShared(int arg) | 共享方式，arg为释放锁的次数，尝试释放资源，如果释放后允许唤醒后续等待结点返回true，否则返回false |

ReentrantLock的加锁和解锁流程

加锁：

- 通过ReentrantLock的加锁方法Lock加锁操作，会调用到内部类Sync的Lock方法。
- 由于Sync的lock方法是抽象方法，所以ReentrantLock通过FairSync和NonfairSync继承Sync类对Lock进行实现。
- 在lock中尝试修改AQS中的state状态，成功则设置线程拥有，失败则走AQS的acquire方法。
- 而acquire方法最后会调用tryAcquire方法，tryAcquire是获取锁的逻辑，获取失败后加入到等待队列中，所以不同的锁类型可以通过重写AQS中的TryAcquire方法，进行实现。

解锁：

通过ReentrantLock的Unlock方法进行解锁，Unlock会调用内部类Sync的Release方法，该方法是AQS中的。

Release中会调用tryRelease方法，该方法原本在AQS中，在Sync类进行重写。获取锁的过程不需要区分锁的类型



线程加入等待队列

当执行Acquire（1）时，会通过tryAcquire获取锁，如果获取锁失败的话，就会调用addWaiter加入到等待队列中去。

```java
private Node addWaiter(Node mode){
    Node node = new Node(Thread.currentThread(),mode);
    Node pred = tail;
    if(pred != null){
        node.prev = pred;
        if(compareAndSetTail(pred,node)){
            pred.next = node;
            return node;
        }
    }
    enq(node);
    return node;
}

private final boolean compareAndSetTail(Node expect, Node update) {
	return unsafe.compareAndSwapObject(this, tailOffset, expect, update);
}

private Node enq(final Node node){
    for ( ; ; ) {
        Node t = tail;
        if (t == null) { // Must initialize
            if (compareAndSetHead(new Node()))
                tail = head;
        } else {
            node.prev = t;
            if (compareAndSetTail(t, node)) {
                t.next = node;
                return t;
            }
        }
    }
}
```

主要流程如下：

通过当前的线程和锁模式新建一个节点，pred指针指向尾结点tail，将new中node的prev指针指向pred

通过compareAndSetTail方法完成尾结点的设置，这个方法主要是对tailOffset和expect进行比较，如果tailOffset的Node和Except的Node地址是相同的，那么设置tail的值为update的值

若果pred指针是null（说明等待队列中没有元素），或者当前pred指针和tail指针的位置不同（说明被别的线程已经修改了），就需要这行enq方法。

如果没有被初始化，需要进行初始化一个头节点。初始化的头节点并不是当前线程节点，而是调用了无参构造函数的节点。如果经历了初始化或者并发导致队列中有元素，则与之前的方法相同。其实，addWaiter就是一个在双端链表添加尾结点的操作，需要注意的是，双端链表的头节点是一个无参构造函数的头节点

总结一下，线程获取锁的时候，过程大体如下:

1. 当没有线程获取到锁时，线程1获取锁成功
2. 线程2申请锁，但是锁被线程1占有

hasQueuedPredecessors是公平锁判断等待队列中是否存在有效节点的方法，如果返回false说明当前线程可以争取共享资源，如果返回true说明队列中存在有效节点，当前线程必须加入到等待队列中。

```java
public final boolean hasQueuedPredecessors(){
    Node t = tail;
    Node h = head;
    Node s;
    return h != t && ((s = h.next) == null || s.thread != Thread.currentThread());
}
```

其中在`h != t && ((s = h.next) == null || s.thread != Thread.currentThread())`中，为什么要判断头节点的下一个节点呢？第一个节点存储的数据是什么？

> 双向链表中，第一个节点为虚节点，其实并不存储任何信息，只是占位，真正的第一个有数据的节点，是在第二个节点开始的。当`h !=t`时：
>
> - 如果(s = h.next) == null,等待队列正在有线程进行初始化，但是只是进行到了tail指向head，没有将head指向tail，此时队列中有元素，需要返回true。
>
> - 如果(s = h.next) != null，说明此时队列中至少有一个有效节点。
>   - 如果此时s.thread == Thread.currentThread()，说明等待队列的第一个有效节点中的线程与当前线程相同，那么当前线程是可以获取资源的。
>   - 如果此时s.thread != Thread.currentThread()，说明等待队列的第一个有效节点线程与当前线程不同，当前线程必须加入进等待队列。

等待队列中线程出队列的时机

```java
public final void acquire(int arg){
    if(!tryAcquire(arg) && acquireQueued(addWaiter(Node.EXCLUSIVE),arg))
        selfInterrupt();
}

addWaiter方法其实就是把对应的线程以node的数据结构形式加入到双端队列中，返回的是一个包含该线程的Node，而这个Node会作为参数，进入到acquireQueued方法中，acquireQueued方法可以对排队中的线程进行获锁操作，直到获取成功或者不需要获取（中断）。

​```java
final boolean acquireQueued(final Node node,int arg){
    //标记是否成功拿到资源
    boolean failed = true;
    try{
        //标记等待过程中是否中断过
        boolean interrupted = false;
        //开始自旋，要么获取锁，要么中断
        for( ; ; ){
            //获取当前节点的前驱节点
            final Node p = node.predecessor();
            //如果p是头节点，说明当前节点在队列的首部，尝试获取锁（头节点是虚节点）
            if(p == head && tryAcquire(arg)){
                //获取锁成功，头指针移动到当前node
                setHead(node);
                p.next = null;
                failed = false;
                return interrupted;
            }
            //说明p为头节点且没有获得锁（可能是非公平锁被抢占了）或者是p不为头节点，这个时候就要判断当前node是否要被阻塞（被阻塞条件：前驱节点的waitStatus为-1），防止无限循环浪费资源
            if(shouldParkAfterFailedAcquire(p,node)&&parkAndCheckInterrupt())
                interrupted = true;
        }
    }finally{
        if(failed)
            cancelAcquire(node);
    }
}


private void setHead(Node node){
	head = node;
    node.thread = null;
    node.prev = null;
}

//靠前驱节点判断当前线程是否应该被阻塞
private static boolean shouldParkAfterFailedAcquire(Node pred,Node node){
    //获取头节点的节点状态
    int ws = pred,waitStatus;
    //说明头节点处于唤醒状态
    if(ws == Node.SIGNAL)
        return true;
    //通过枚举值知道waitStatus>0是取消状态
    if(ws > 0){
    	do{
            //循环向前查找取消节点，把取消节点从队列中删除
            node.prev = pred = pred.prev;
        }while(pred.waitStatus > 0){
            pred.next = node;
        }
    }else{
        //设置前人节点等待任务状态为SIGNAL
        compareAndSetWaitStatus(pred,ws,Node.SIGNAL);
    }
    return false;
}

//主要用于挂起当前线程，阻塞调用栈，返回当前线程的中断状态
private final boolean parkAndCheckInterrupt(){
    LockSupport.park(this);
    return Thread.interrupted();
}
```

上述方法的流程：

![img](https://p0.meituan.net/travelcube/c124b76dcbefb9bdc778458064703d1135485.png)

跳出当前循环的条件是当“前置节点是头节点，且当前线程获取锁成功”。为了防止因死循环导致CPU资源浪费，我们会判断前置节点的状态来决定是否要将当前线程挂起，具体挂起流程如下：

![img](https://p0.meituan.net/travelcube/9af16e2481ad85f38ca322a225ae737535740.png)

CANCELLED状态节点生成

acquireQueued方法中的finally块中会判断failed的值，true则走cancelAcquire（node）方法

```java
private void cancelAcquire(Node node){
    //无效节点过滤
    if(node == null)
        return;
    //设置该节点不关联任何线程
    node.thread = null;
    Node pred = node.prev;
    //通过前驱节点，跳过取消状态的node
    while(pred.waitStatus > 0)
        node.prev = pred = pred.prev;
    //获取过滤后的前驱节点的后继节点
    Node predNext = pred.next;
    //把当前node的转态设置为CANCELLED
    node.waitStatus = Node.CANCELLED;
    //如果当前节点是尾结点，将从后往前的第一个非取消状态的节点设置为尾结点，更新失败的话，则进入else，如果更新成功，将tail的后继节点设置为null
    if(node == tail && compareAndSetTail(node,pred)){
        compareAndSetNext(pred,predNext,null);
    }else{
        int ws;
        //如果当前节点不是head的后继节点：1、判断当前节点前驱节点是否为SIGNAL；2、如果不是，则把前驱节点设置为SIGNAL看是否成功。如果1和2中有一个为true，在判断当前节点的线程是否为null；如果上述条件否满足，把当前节点的前驱节点的后继指针指向当前节点的后继节点
        if(pred != head && 
           ((ws = pred.waitStatus) = Node.SIGNAL || 
            (ws <= 0 && compareAndSetWaitStatus(pred,ws,Node.SIGNAL))) && 
           pred.thread != null){
            Node next = node.next;
            if(next != null && next.waitStatus <= 0)
                compareAndSetNext(pred,predNext,next);
        }else{
            //如果当前节点是head的后继节点，或者上述条件不满足，那就唤醒当前节点的后继节点
            unparkSuccessor(node);
        }
        node.next = node;
    }
}
```

当前流程：

- 获取当前节点的前驱节点，如果前驱节点的状态是CANCELLED，那就一直往前遍历，找到第一个waitStatus <= 0的节点，将找到的pred节点和当前Node关联，将当前Node设置为CANCELLED
- 根据当前节点的位置，考虑以下三种情况：
  1. 当前节点是尾结点
  2. 当前节点是head的后继节点
  3. 当前节点不是head的后继节点，也不是尾结点



如何解锁

```java
public void unlock(){
    sync.release(1);
}
```

在ReentrantLock中本质解锁是通过AQS来实现的

```java
public final boolean release(int args){
    //实现的tryRelease如果返回true，说明该锁没有被任何线程持有
    if(tryRelease(arg)){
        //获取头节点
        Node h = head;
        //头节点不为空并且头节点的waitStatus不是初始化节点情况，解除线程挂起状态
        if(h != null && h.waitStatus != 0)
            unparkSuccessor(h);
        return true;
    }
    return false;
}

private void unparkSuccessor(Node node) {
	// 获取头结点waitStatus
	int ws = node.waitStatus;
	if (ws < 0)
		compareAndSetWaitStatus(node, ws, 0);
	// 获取当前节点的下一个节点
	Node s = node.next;
	// 如果下个节点是null或者下个节点被cancelled，就找到队列最开始的非cancelled的节点
	if (s == null || s.waitStatus > 0) {
		s = null;
		// 就从尾部节点开始找，到队首，找到队列第一个waitStatus<0的节点。
		for (Node t = tail; t != null && t != node; t = t.prev)
			if (t.waitStatus <= 0)
				s = t;
	}
	// 如果当前节点的下个节点不为空，而且状态<=0，就把当前节点unpark
	if (s != null)
		LockSupport.unpark(s.thread);
}

在ReentrantLock里面的公平锁和非公平锁的父类Sync中定义了可重入锁释放锁的机制

​```java
//方法返回当前锁是不是没有线程持有
protected final boolean tryRelease(int releases){
    //减少可重入次数
    int c = getState() - releases;
    //当前线程不是持有锁的线程，抛出异常
    if(Thread.currentThread() != getExclusiveOwnerThread())
        throw new IllegalMonitorStateException();
    boolean free = false;
    //如果持有线程全部释放，将当前独占锁所有线程设置为null,并更新state
    if(c == 0){
        free = true;
        setExclusiveOwnerThread(null);
    }
    setState(c);
    return free;
}
```



处于排队等候机制中的线程，什么时候才有机会获取锁？

如果处于排队等候机制中的线程一直无法获取锁，需要一直等待么？有没有别的策略来解决这一个问题？









ReentrantLock结构分析

```java
public class ReentrantLock implement Lock{
    
    final Sync sync;
    
    abstract static class Sync extends AbstractQueuedSynchronizer{
        
        private abstract void lock();
        
        final boolean nonfairTryAcquire(int acquire){}
        //独占方式，arg为释放锁的次数，参数释放资源，成功返回true，失败返回false
        protected final boolean tryRelease(int release){}
        //该线程是否正在独占资源
        protected final boolean isHeldExclusively(){
            return getExclusiveOwnerThread() == Thread.currentThread();
        }
        
        final ConditionObject newCondition(){
            return new ConditionObject();
        }
        
        final Thread getOwner(){
            return getState() == 0 ? null : getExclusiveOwnerThread();
        }
        
        final int getHoldCount(){
            return isHeldExclusively() ? getState():0;
        }
        
        final boolean isLocked(){
            return getState() != 0;
        }
        
        private void readObject(ObjectInputStream s){
            throw java.io.IOException,ClassNotFoundException{
                s.defaultReadObject();
                setState(0);
            }
        }
        
    }
    
    final class NonfairSync extends Sync{
        
        final void lock(){
            if(compareAndSetState(0,1))
                setExclusiveOwnerThread(Thread.currentThread());
            else
                acquire(1);
        }
        
        protected final boolean tryAcquire(int acquires){
            return nonfairTryAcuire(acquires);
        }
        
    }
    
    final class FairSync extends Sync{
        
        final void lock(){
            acquire(1);
        }
        
        protected final boolean tryAcquire(int acquires){}
    }
    
    public ReentrantLock(){
        sync = new NonfairSync();
    }
    
    public ReentrantLock(boolean fair){
        sync = fair ? new FairSync() : new NonfairSync();
    }
    
    public void lock(){
        Sync.lock();
    }
    
    public void lockInterruptibly(){
        sync.acquireInterruptibly(1);
    }
    
    public boolean tryLock(){
        return sync.nonfairTryAcquire(1);
    }
    
    public boolean tryLock(long timeout,TimeUnit unit){
        return sync.tryAcquireNanos(1,unit.toNanos(timeout));
    }
    
    public void unlock(){
        sync.release(1);
    }
    
    public Condition newCondition(){
        return sync.newCondition();
    }
    
    public int getHoldCount(){
        return sync.getHoldCount();
    }
    
    public boolean isHeldByCurrentThread(){
        return sync.isHeldExclusively();
    }
    
    public boolean isLocked(){
        return sync.isLocked();
    }
    
    public final boolean isFair(){
        return sync instanceof FairSync;
    }
    
    protected Thread getOwner(){
        return sync.getOwner();
    }
    
    public final boolean hasQueuedThread(){
        return sync.hasQueuedThreads();
    }
    
    public final boolean hasQueuedThread(Thread thread){
        return sync.isQueued(thread);
    }
    
    public final int getQueueLength(){
        return sync.getQueueLength();
    }
    
    protected Collection<Thread> getQueuedThreads(){
        return sync.getQueuedThreads();
    }
    
    public boolean hasWaiters(Condition condition){}
    
    public int getWaitQueueLength(Condition condition){}
    
    protected Collection<Thread> getWaitingThreads(Condition condition){}
    
    public Stirng toString(){}
    
}
```



AbstractQueuedSynchronizer结构分析

```java
public abstract class AbstractQueuedSynchronizer extend AbstractOwnableSynchronizer{
    
    protected AbstractQueuedSynchronizer(){}
    
    static final class Node{
        //表示线程以共享的模式等待锁
        static final Node SHARE = new Node();
        //表示线程以独占的方式等待锁
        static final Node EXCLUSIVE = null;
        //表示线程获取锁的请求已经取消了
        static final int CANCELLED = 1;
        //表示线程已经准备好了，等待资源释放
        static final int SIGNAL = -1;
        //表示节点在等待队列中，节点线程等待唤醒
        static final int CONDITION = -2;
        //表示当前线程出于SHARED情况下
        static final int PROPAGATE = -3;
        //当前节点在队列中的状态，初始化的默认值为0
        volatile int waitStatus;
        //前驱指针
        volatile Node prev;
        //后继指针
        volatile Node next;
        //表示处于该节点的线程
        volatile Thread thread;
        //指向下一个处于CONDITION状态的节点
        Node nextWaiter;
        
        final boolean isShared(){
            return nextWaiter = SHARED;
        }
        //返回前驱节点，没有的话抛出异常
        final Node Predecessor(){
            Node p = prev;
            if(p == null)
	            throw new NullPointException();
            else
                return p;
        }
        
        Node(){}
        
        Node(Thread thread,Node node){
            this.nextWaiter = node;
            this.thread = thread;
        }
        
        Node(Thread thread,int waitStatus){
            this.waitStatus = waitStatus;
            this.thread = thread;
        }
    }
    
    private transient volatile Node head;
    
    private transient volatile Node tail;
    //同步状态
    private volatile int state;
    //获取State的值
    protected final int getState(){
        return state;
    }
    //使用CAS的方式更新State
    protected final void setState(int newState){
        state = newState;
    }
    
    protected final boolean compareAndSetState(int expect,int update){
        return unsafe.compareAndSwapInt(this.stateOffest,expect,update);
    }
    
    static final long spinForTimeoutThreshold = 1000L;
    
    private Node enq(final Node node){
        for(;;){
            Node t = tail;
            if(t == null){
                if(compareAndSetHead(new Node()))
                    tail = head;
            }else{
                node.prev = t;
                if(compareAndSetTail(t,node)){
                    t.next = node;
                    return t;
                }
            }
        }
    }
    
    private Node addWaiter(Node node){}
    
    private void setHead(Node node){}
    
    private void unparkSuccessor)(Node node){}
    
    private void daReleaseShared(){}
    
    private void setHeadAndPropagate(Node node,int propagate){}
    
    private void cancelAcquire(Node node){}
    
    private static boolean shouldParkAfterFailedAcquire(Node pred,Node node){}
    
    static void selfInterrupt(){
        Thread.currentThread().interruptp();
    }
    
    private final boolean parkAndCheckInterrupt(){}
    
    final boolean acquireQueued(final Node node,int arg){}
    
    private void doAcquireInterruptibly(int arg){}
    
    private boolean doAcquireNanos(int arg,long nanosTimeout){}
    
    private void doAcquireShared(int arg){}
    
    private void doAcquireSharedInterruptibly(int arg){}
    
    private boolean doAcquireShareNanos(int arg,long nanosTimeout){}
    
    protected boolean tryAcquire(int arg){
        throw new UnsupportedOperationException();
    }
    
    protected boolean tryRelease(int arg){
        throw new UnsupportOperationException();
    }
    
    protected int tryAcquireShared(int arg){
        throw new UnsupportedOperationException();
    }
    
    protected boolean tryReleaseShared(int arg){
        throw new UnsupportedOperationException();
    }
    
    protected boolean isHeldExclusively(){}
    
    public final void acquire(int arg){
        if(!tryAcquire(arg) && acquireQueued(addWaiter(Node.EXCLUSIVE),arg))
            slefInterrupt();
    }
    
    public final void acquireInterruptibly(int arg){}
    
    public final boolean tryAcquireNanos(int arg,long nanosTimeout){}
    
    public final boolean release(int arg){}
    
    public final void acquireShared(int arg){}
    
    public final void acquireSharedInterruptibly(int arg){}
    
    public final boolean tryAcquireShareNanos(int arg,long nanosTimeout){}
    
    public final boolean releaseShared(int arg){}
    
    public final boolean hasQueuedThreads(){
        return head != tail;
    }
    
    public final boolean hasContendef(){
        return head != null;
    }
    
    public final Thread getFirstQueuedThread(){}
    
    private Thread fullGetFirstQueuedThread(){}
    
    public final boolean isQueued(Thread thread){}
    
    final boolean apparentlyFirstQueuedIsExclusive(){}
    
    public final boolean hasQueuedPredecessors(){}
    
    public final int getQueueLength(){}
    
    public final Collection<Thread> getQueuedThreads(){}
    
    public final Collection<Thread> getExclusiveQueuedThreads(){}
    
    public final Collection<Thread> getSharedQueuedThreads(){}
    
    public String toStirng(){}
    
    final boolean isOnSyncQueue(Node node){}
    
    private boolean finaNodeFromTail(Node node){}
    
    final boolean transferForSignal(Node node){}
    
    final boolean transferAfterCancelledWait(Node node){}
    
    final int fullyRelease(Node node){}
    
    public final boolean owns(ConditionObject condition){}
    
    public final boolean hasWaiters(ConditionObject condition){}
    
    public final int getWaitQueueLength(ConditionObject condition){}
    
    public final Collection<Thrad> getWaitingThreads(ConditionObject condition){}
    
    public class ConditionObject implements Condition{
    
        private transient Node firstWaiter;
        
        private transient Node lastWaiter;
        
        public ConditionObject(){}
        
        private Node addConditionWaiter(){}
        
        private void doSignal(Node first){}
        
        private void doSignalAll(Node first){}
        
        private void unlinkCancelledWaiters(){}
        
        public final void signal(){}
        
        public final void signalAll(){}
        
        public final void awaitUninterruptibly(){}
        
        private static final int REINTERRUPT = 1;
        
        private static final int THROW_IE = -1;
        
        private int checkInterruptWhileWaition(Node node){}
        
        private void reportInterruptAfterWait(int interruptMode){}
        
        public final void await(){}
        
        public final long awaitNanos(long nanosTimeout){}
        
        public final boolean awaitUntil(Date deadline){}
        
        public final boolean await(long time,TimeUnit unit){}
        
        final boolean isOwnedBy(AbstractQueuedSynchronizer sync){}
        
        protected final boolean hasWaiters(){}
        
        protected final int getWaitQueueLength(){}
        
        protected final Collection<Thread> getWaitingThreads(){}
        
    }
    
    private static final Unsafe unsafe = Unsafe.getUnsafe();
    
    private static final long stateOffset;
    
    private static final long headOffset;
    
    private static final long tailOffset;
    
    private static final long waitStatusOffset;
    
    private static final long nextOffset;
    
    static{
        try{
            tateOffset = unsafe.objectFieldOffset
                (AbstractQueuedSynchronizer.class.getDeclaredField("state"));
            headOffset = unsafe.objectFieldOffset
                (AbstractQueuedSynchronizer.class.getDeclaredField("head"));
            tailOffset = unsafe.objectFieldOffset
                (AbstractQueuedSynchronizer.class.getDeclaredField("tail"));
            waitStatusOffset = unsafe.objectFieldOffset
                (Node.class.getDeclaredField("waitStatus"));
            nextOffset = unsafe.objectFieldOffset
                (Node.class.getDeclaredField("next"));
        }catch(Exception e){
            throw new Error(e)
        }
    }
    
    private final boolean compareAndSetHead(Node update){}
    
    private final boolean compareAndSetTail(Node expect, Node update){}
    
    private static final boolean compareAndSetWaitStatus(Node node,int expect,int update){}
    
    private static final boolean compareAndSetNext(Node node,Node expect,Node update){}
    
}




```