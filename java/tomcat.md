# java web

## 一、Tomcat

### 网站流程

​	1、在浏览器输入一个域名，回车

​	2、检测本机的C:\\Windows\System32\drivers\etc\hosts配置文件下面有没有这个域名映射

​		有：直接返回对应的ip地址，这个地址中，有我们需要访问的web程序，可以直接访问

​		没有：去DNS服务器寻找，找到的话返回，找不到的话报错

![image-20200809122254625](C:%5CUsers%5Cclsld%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20200809122254625.png)

### 发布一个web网站

​	将自己写的网站，放到服务器（Tomcat）中指定的web应用的文件夹（webapps）下，就可以访问网站该有的结构

```java
--webapps：Tomcat服务器的web目录
    -root
    -clsld：网站的目录名
    	-web-INF
    		-classes：java程序
    		-lib：web应用所依赖的jar包
    		-web.xml：网站配置文件
    	-index.html 默认的首页
    	-static
    		-css
    			-style.css
    		-js
    		-img
    	-......
```

## 二、HTTP

### 	2.1、什么是HTTP

​	HTTP是一个简单的请求-响应协议，它通常运行在tcp之上。他指定了客户端可能发送给服务器什么样的消息以及得到什么样的响应。请求和响应消息的头以ascll码形式给出；而消息内容则具有一个类似MIME的格式。这个简单模型是早期web成功的功臣，因为它使得开发和部署是那么的直接了当

## 三、servlet

### 3.1、servlet简介

- servlet就是公司开发动态web的一门技术

- sun在这些api中提供了一个借口叫做：servlet，如果你想开发一个servlet程序，只需完成两个小步骤：
  - 编写一个类，实现servlet接口
  - 把开发好的java类部署到web服务器中
- 把实现了servlet接口的java查询叫做servlet

### 3.2、helloServlet

1、构建一个普通的maven项目，删除里面的src目录，以后我们就在这个项目建立moudel，这个空的工程就是maven的主工程