注解就是代码中特殊标记，这些标记可以在编译、类加载、运行时被读取，并执行相应的处理。

注解其实在开发中非常常见，比如我们使用各种框架时（像spring框架就会有@Controller、@Param、@Select等），一些项目也会用到lombok注解，@Slf4j、@Data等等，除了框架实现的注解，java原生也有@Override、@Deprecated、@Functional、@Interface等基本注解，不过java原生的基本注解大多数用于标记和检查，java除了提供基本注解之外，还有一种叫做元Annotation（元注解），所谓的元Annotation有@Retention和@Target。@Retention注解可以简单理解为设置注解的生命周期，而@Target表示这个注解可以修饰哪些地方（比如方法、还是成员变量、还是包等等）

对于基础的监控信息，显然可以通过AOP切面的方式去处理。要自定义注解，首先考虑我们是在什么时候解析这个注解，这就需要用到前面说的@Retention注解，这个注解会修饰我们自定义注解生命周期。@Retention注解传入的是RetentionPolicy枚举，该枚举有三个常量，分别是SOURCE、CLASS和RUNTIME。如果你想在编译期间处理注解相关的逻辑，你需要继承AbstractProcessor并实现process方法，比如可以看到lombok就用到了AnnotationProcessor继承了AbstractProcessor。一般来说，只要自定义的注解中@Retention注解设置为SOURCE和CLASS这两个级别，那么就需要继承并实现。因为SOURCE和CLASS这两个级别等待加载到jvm的时候，注解就被抹除了。lombok的实现原理就是在这（为什么使用了个@Data这样的注解就能有set/get等方法，就是在这里加上去的）https://blog.mythsman.com/post/5d2c11c767f841464434a3bf/













在java中泛型简单来说就是在创建对象或者调用方法的时候才明确下具体的类型。使用泛型的好处就是使代码更加简洁（不需要强制类型转换），程序更加健壮（在编译器间没有警告，在运行期间不会出现ClassCastException异常），泛型在操作集合的时候用得比较多`List lists = new ArrayList<>();lists.add("面试造火车");`,还有会在编写【基础组件】的时候会用到，组件为了做到足够的通用性，是不知道用户传入什么类型参数进来的，所以在这种情况下用泛型就是很好的办法。这块可以参考SpringData JPA和JpaRepository的写法。要写组件还是离不开java反射机制（能够从运行时获取信息），所以一般组件是泛型+反射来实现的。

背景：这边有个需求，需要根据某些字段进行聚合。换到SQL其实就是select sum(column1),sum(column2)from table group by fiold1,field2;需要sum和group by的列肯定是由业务方自己传入的，而sql表其实就是我们自己的pojo（传入的字段也是pojo的属性），单个业务实际上可以在参数上写死pojo，但是为了做得更加通用，我把入参设置为泛型，拿到参数后，通过反射获取其字段具体的值做累加就好了。具体代码如下：

```java
public cacheMap(List<String> groupByKeys, List<String> sumValues) { 
    this.groupByKeys = groupByKeys;  this.sumValues = sumValues;
}

private void excute(T e) {    
    // 从pojo 取出需要group by 的字段 
    list  List<Object> key = buildPrimaryKey(e);
    //primaryMap是存储结果的Map  
    T value = primaryMap.get(key);
    // 如果从存储结果找到有相应记录  
    if (value != null) {
        for (String elem : sumValues) {      
            // 反射获取对应的字段，做累加处理      
        Field field = getDeclaredField(elem, e);      
        if (field.get(e) instanceof Integer) {    field.set(value, (Integer) field.get(e) + (Integer) field.get(value));      } else if (field.get(e) instanceof Long) {                field.set(value, (Long) field.get(e) + (Long) field.get(value));      } else {
                throw new RuntimeException("类型异常,请处理异常");      
            } 
        } 
        // 处理时间记录    
        Field field = getDeclaredField("updated", value);    
        if (null != field) {  
            field.set(value, DateTimeUtils.getCurrentTime());
        }  
    } else { 
        // group by 字段 第一次进来   
        try {      
            primaryMap.put(key, Tclone(e));
            createdMap.put(key, DateTimeUtils.getCurrentTime()); 
        }catch (Exception ex) { 
            log.info("first put value error {}" , e);   
        }
    }
}

```



