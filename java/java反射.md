# java反射

简单来说，反射就是java可以在给我们运行时获取类的信息，理解反射重点就在于理解说明是【运行时】，为什么我们要在运行时获取类的信息。在运行是获取类的信息其实是为了让我们所写的代码更具有【通用性】和【灵活性】，要理解反射，需要抛开我们日常写的业务代码，以更高的未读或者说是抽象的思维去看待我们所写的“工具”。所谓的工具：在单个系统使用叫做“utils”，被多个系统使用打成jar包叫做“组件”、组件继续发展壮大就叫做“框架”。你肯定不知道用该工具的用户传入的是什么对象，但是你需要帮他们得到需要的结果。例如在SpringMVC我们在方法上写上对象，传入的参数会帮你封装到对象上。mybatis可以让我们只写接口，不写实现类，就可以执行SQL，我们在类加上@Component注解，Spring就帮我们创建对象，这些通通都有反射的身影：约定大于配置，配置大于硬编码。通过约定使用的方式，使用反射在运行时获取相应的信息，实现代码功能的【通用性】和【灵活性】

泛型的信息只是存在编译阶段，在class字节码就看不见泛型的信息了，那么为什么下面这段代码能够获得到泛型的信息呢？

```java
public abstract class BaseDao<T>{
    public BaseDao(){
        Class clazz = this.getclass();
        ParameterizedType pt = (ParameterizedType) clazz.getGenericSuperclass();
        clazz = (Class)pt.getActualTypeArguments()[0];
        System.out.println(clazz);
    }
}
public class UserDao extends BaseDao<User>{
    public static void main(String[] args){
        BaseDao<User> userDao = new UserDao();
    } 
}
class com.entity.User
```

其实是这样的，可以理解为泛型擦除是有范围的，定义在类上的泛型信息是不会被擦除的，java编译器仍在class文件以Signature属性的方式保留了泛型信息。Type作为顶级接口，Type下还有几种类型，比如TypeVariable、ParameterizedType、WildCardType、GenericArrayType、以及Class。通过这些接口我们可以在运行时获取泛型相关的信息。

动态代理其实是代理模式的一种，代理模式是设计模式之一。代理模式有静态代理和动态代理，静态代理需要自己写代理类，实现对应的接口，比较麻烦。在java中，动态代理常见的又有两种实现方式：jdk动态代理和CGLIB代理。jdk动态代理其实就是运用了反射的机制，而CGLIB代理则用的是利用ASM框架，通过修改器字节码生成子类来处理。jdk动态代理会帮我们实现接口的方法，通过invokeHandler对所需要的方法进行增强。动态代理这一技术在实际或者框架原理中是非常常见的。像上面所讲得mybatis不用写实现类，只写接口就可以执行sql，又或者是SpringAOP等等好用的技术，实际上用得都是动态代理。

