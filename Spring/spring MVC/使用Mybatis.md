## mybatis简介

**MyBatis 是一款优秀的持久层框架，它支持定制化 SQL、存储过程以及高级映射。MyBatis 避免了几乎所有的 JDBC 代码和手动设置参数以及获取结果集。MyBatis 可以使用简单的 XML 或注解来配置和映射原生信息，将接口和 Java 的 POJOs(Plain Old Java Objects,普通的 Java对象)映射成数据库中的记录。**

## 使用xml配置例子

### **引入jar包**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>20200703_IOC</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>HelloMybatis</artifactId>
    <dependencies>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.3</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.19</version>
        </dependency>
    </dependencies>
</project>
```

### **项目目录结构**

![image-20200711122603836](C:\Users\clsld\AppData\Roaming\Typora\typora-user-images\image-20200711122603836.png)

个人总结，xml配置文件要放在resources文件目录下才能被识别，可能还有其他的build path的方式可以解决这个问题。

### **student实体类与数据库表参数映射**

![image-20200711122750038](C:\Users\clsld\AppData\Roaming\Typora\typora-user-images\image-20200711122750038.png)

#### **student.java**

```java
package pojo;
public class student{
    int id;
    int studentid;
    String name;
//下面省略的是getter and setter方法,构造方法和空构造方法,toString方法
```

#### **studentDao.java**

```java
package pojo;
import java.util.List;
public interface studentDao {
    //下面的函数对应着mapper里面各种SQL方法标签的id
    //每个方法于id一一对应
     List<student> listStudent();
     void addstudent(student student);
     void deletestudent(student student);
     student getstudent(int id);
     void updatestudent(student student);
}
```

#### **mybatis-config.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE configuration PUBLIC "-//mybatis.org//DTD Config 3.0//EN" "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <typeAliases>
        <package name="pojo"/>
    </typeAliases>
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC">
            </transactionManager>
            <dataSource type="POOLED">
                <property name="driver" value="com.mysql.cj.jdbc.Driver"/>
                //mysql 驱动由com.mysql.jdbc.Driver变为com.mysql.cj.jdbc.Driver
                <property name="url" value="jdbc:mysql://localhost:3306/mybatis?characterEncoding=UTF-8&amp;serverTimezone=UTC"/>//mysql 80要配置时区问题
                <property name="username" value="root"/>
                <property name="password" value="root"/>
            </dataSource>
        </environment>
    </environments>
    <mappers>
        <mapper resource="student.xml"/>
        //要映射的配置文件，映射此文件后才能让mybatis知道要折行的SQL方法在哪
    </mappers>
</configuration>
```

#### **student.xml**

```xml
<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
//在这里面编写id类的SQL语句、传参类型和返回类型
<mapper namespace="pojo.studentDao">
    <select id="listStudent" resultType="student">
        select * from student
    </select>
    <insert id="addstudent" parameterType="student">
        insert into student (id,studentid,name) values (#{id},#{studentid},#{name})
    </insert>
    <delete id="deletestudent" parameterType="student">
        delete from student where id = #{id}
    </delete>
    <select id="getstudent" parameterType="int" resultType="student">
        select * from student where id = #{id}
    </select>
    <update id="updatestudent" parameterType="student">
        update student set name = #{name} where id = #{id};
    </update>
</mapper>
```

#### TestMybatis.java

```java
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import pojo.student;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
public class TestMybatis {
    public static void main(String[] args) throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = sqlSessionFactory.openSession();
        //查询所有用户
        List<student> listStudent = session.selectList("listStudent");
        for (student student :listStudent){
            System.out.println("ID:"+student.getId()+",NAME:"+student.getName());
        }
        //加入一个用户
        student s1 = new student();
        s1.setId(4);
        s1.setStudentid(4444);
        s1.setName("clsld");
        session.insert("addstudent",s1);
        //查询一个用户
        student s2 = session.selectOne("getstudent",4);
        System.out.println(s2.toString());
        //更新用户
        s1.setName("zzz");
        session.update("updatestudent",s1);
        //查询一个用户
        s2 = session.selectOne("getstudent",4);
        System.out.println(s2.toString());
    }
}
```



## 使用注解配置例子

**其他内容不变还是和上面的一样，主要改变的有studentDao和mybatis-config.xml里面的内容**

### studentDao.java

```java
package pojo;
import org.apache.ibatis.annotations.*;
import java.util.List;
public interface studentDao {
     @Select("SELECT * FROM student ")
     List<student> listStudent();
     @Insert("INSERT INTO student (id,studentid,name) VALUES (#{id},#{studentid},#{name})")
     void addstudent(student student);
     @Delete("DELETE FROM student WHERE id = #{id}")
     void deletestudent(student student);
     @Select("SELECT * FROM student WHERE id = #{id}")
     student getstudent(int id);
     @Update("UPDATE student SET name = #{name} WHERE id = #{id};")
     void updatestudent(student student);
}
```

### mybatis-config.xml

改变主要是将之前的mapper里面关联配置文件那一句给注释掉，并且重写编写mapper关联类

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE configuration PUBLIC "-//mybatis.org//DTD Config 3.0//EN" "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <typeAliases>
        <package name="pojo"/>
    </typeAliases>
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC">
            </transactionManager>
            <dataSource type="POOLED">
                <property name="driver" value="com.mysql.cj.jdbc.Driver"/>
                <property name="url" value="jdbc:mysql://localhost:3306/mybatis?characterEncoding=UTF-8&amp;serverTimezone=UTC"/>
                <property name="username" value="root"/>
                <property name="password" value="root"/>
            </dataSource>
        </environment>
    </environments>
<!--    <mappers>-->
<!--        <mapper resource="student.xml"/>-->
<!--    </mappers>-->
    <mappers>
        <mapper class="pojo.studentDao"></mapper>
    </mappers>
</configuration>
```

之前的配置文件student.xml因为没有关联，所以在这个程序中并没有起作用，可以删除掉也可以不理会。

