## Spring MVC

**为何要使用spring？**

- **方便解耦，便于开发（spring就是一个大工厂，可以将所有对象的创建和依赖关系维护交给spring管理）**
- **支持aop编程，可以很方便的实现对程序进行权限拦截和运行监控**
- **可以通过配置完成对事务的支持**
- **方便程序的测试，支持对unit4支持，可以通过注解方便的spring程序**
- **封装了一些api，例如jdbc等**

### 一、什么是MVC

- MVC是model、view、controller的简写，是一种软件设计规范

- 是将业务逻辑、数据、显示分离出来的方法来组织代码

- MVC降低了视图与业务逻辑间的双向耦合
  - 模型（dao,service）
  - 视图（jsp）
  - 控制器（servlet）

- 最经典的MVC就是jsp+servlet+javabean的模式

  - dao层 连接数据库

  - service层 service调dao层去实现具体的业务 只管业务

  - servlet层 （转发，重定向）接受前端的数据，传给service，

    处理完后通过servlet返回给指定的前端页面

  - jsp/html

### 二、回顾servlet

1. 新建maven普通项目名为Servlet，删除src目录，创建新moudel也是普通项目名为test01。

   ![image-20200812153447800](C:%5CUsers%5Cclsld%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20200812153447800.png)

2. 可在Servlet的pom.xml配置jar包，以后就有相同的jar包就可以不用再moudel里的jar包里面配置，也可在moudel里的pom.xml配置jar包，当创建其他moudel的时候就不通用了

   以我为例是在moudel的pom.xml里配置

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <project xmlns="http://maven.apache.org/POM/4.0.0"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
       <parent>
           <artifactId>servlet</artifactId>
           <groupId>org.example</groupId>
           <version>1.0-SNAPSHOT</version>
       </parent>
       <modelVersion>4.0.0</modelVersion>
   
       <artifactId>test01</artifactId>
   <dependencies>
   <!--    单元测试-->
       <dependency>
           <groupId>junit</groupId>
           <artifactId>junit</artifactId>
           <version>4.12</version>
           <scope>test</scope>
       </dependency>
   <!--    这个jar包包括了spring mvc 框架相关的所有类-->
       <dependency>
           <groupId>org.springframework</groupId>
           <artifactId>spring-webmvc</artifactId>
           <version>5.1.13.RELEASE</version>
       </dependency>
   <!--    下面三个包跟jsp有关-->
       <dependency>
           <groupId>javax.servlet</groupId>
           <artifactId>servlet-api</artifactId>
           <version>2.5</version>
       </dependency>
       <dependency>
           <groupId>javax.servlet.jsp</groupId>
           <artifactId>jsp-api</artifactId>
           <version>2.2</version>
       </dependency>
       <dependency>
           <groupId>javax.servlet</groupId>
           <artifactId>jstl</artifactId>
           <version>1.2</version>
       </dependency>
   </dependencies>
   
   <build>
       <plugins>
           <plugin>
               <groupId>org.apache.maven.plugins</groupId>
               <artifactId>maven-compiler-plugin</artifactId>
               <version>3.8.1</version>
               <configuration>
                   <source>12</source>
                   <target>12</target>
               </configuration>
           </plugin>
       </plugins>
   </build>
   </project>
   ```

   `maven`的默认编译使用的`jdk`版本貌似很低，使用`maven-compiler-plugin`插件可以指定项目源码的`jdk`版本，编译后的`jdk`版本，以及`编码`。即加上build这段代码，里面的12是j大家电脑的jdk版本，之后就不会出现找不到发行版本12的问题

3. 为test01添加web模块，在test01文件夹右击选择第二个add framework support,添加web模块

4. 在test01的java文件夹新建com.xxx.Servlet包，在Servlet中新建HelloServlet类

   ```java
   package com.kuang.Servlet;
   
   import javax.servlet.ServletException;
   import javax.servlet.http.HttpServlet;
   import javax.servlet.http.HttpServletRequest;
   import javax.servlet.http.HttpServletResponse;
   import java.io.IOException;
   
   public class HelloServlet extends HttpServlet {
       @Override
       protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
           String method = request.getParameter("method");
   //        获取参数
           if (method.equals("add")) {
               request.getSession().setAttribute("msg", "折行了add方法");
           }
           if (method.equals("delete")) {
               request.getSession().setAttribute("msg", "折行了delete方法");
           }
   //        业务逻辑
   //        视图跳转
           request.getRequestDispatcher("/WEB-INF/jsp/hello.jsp").forward(request,response);
   
       }
       @Override
       protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
           doGet(request,response);
       }
   }
   
   ```

5. 在WEB-INF目录下新建jsp文件夹，在文件夹中建hello.jsp

   ![image-20200812154206210](C:%5CUsers%5Cclsld%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20200812154206210.png)

6. 在web.xml中配置servlet

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
            version="4.0">
       
       <servlet>
           <!--        起个名字，名字最好与servlet层的包对应-->
           <servlet-name>HelloServlet</servlet-name>
           <!--        包的路劲-->
           <servlet-class>com.kuang.Servlet.HelloServlet</servlet-class>
       </servlet>
       <!--    设置servlet的访问参数和与哪个name对应，即与那个包对应-->
       <servlet-mapping>
           <servlet-name>HelloServlet</servlet-name>
           <url-pattern>/hello</url-pattern>
       </servlet-mapping>
   </web-app>
   ```

7. 配置Tomcat，点击Edit Configurations

   ![image-20200812154633319](C:%5CUsers%5Cclsld%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20200812154633319.png)

![image-20200812154852922](C:%5CUsers%5Cclsld%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20200812154852922.png)

### 三、spring MVC折行原理图

![img](https://i.loli.net/2020/12/30/QvISPrHYRsd6Et4.png)

图为SpringMVC的一个较完整的流程图，实线表示SpringMVC框架提供的技术，不需要开发者实现，虚线表示需要开发者实现。

***\*简要分析执行流程\****

1. DispatcherServlet表示前置控制器，是整个SpringMVC的控制中心。用户发出请求，DispatcherServlet接收请求并拦截请求。

   我们假设请求的url为 : http://localhost:8080/SpringMVC/hello

   ***\*如上url拆分成三部分：\****

   http://localhost:8080服务器域名

   SpringMVC部署在服务器上的web站点

   hello表示控制器

   通过分析，如上url表示为：请求位于服务器localhost:8080上的SpringMVC站点的hello控制器。

2. HandlerMapping为处理器映射。DispatcherServlet调用HandlerMapping,HandlerMapping根据请求url查找Handler。

3. HandlerExecution表示具体的Handler,其主要作用是根据url查找控制器，如上url被查找控制器为：hello。

4. HandlerExecution将解析后的信息传递给DispatcherServlet,如解析控制器映射等。

5. HandlerAdapter表示处理器适配器，其按照特定的规则去执行Handler。

6. Handler让具体的Controller执行。

7. Controller将具体的执行信息返回给HandlerAdapter,如ModelAndView。

8. HandlerAdapter将视图逻辑名或模型传递给DispatcherServlet。

9. DispatcherServlet调用视图解析器(ViewResolver)来解析HandlerAdapter传递的逻辑视图名。

10. 视图解析器将解析的逻辑视图名传给DispatcherServlet。

11. DispatcherServlet根据视图解析器解析的视图结果，调用具体的视图。

12. 最终视图呈现给用户。

> 以上解释来源于狂神说java

### 四、第一个springMVC程序

1. 首先在原来的project的基础上新建一个moudel名为springmvc-01，并添加为web项目

2. 新建包com.kuang.controller,新建类HelloController

3. 在pom.xml加入编译判断

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <project xmlns="http://maven.apache.org/POM/4.0.0"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
       <parent>
           <artifactId>servlet</artifactId>
           <groupId>org.example</groupId>
           <version>1.0-SNAPSHOT</version>
       </parent>
       <modelVersion>4.0.0</modelVersion>
   
       <artifactId>springmvc-01</artifactId>
   
       <build>
           <plugins>
               <plugin>
                   <groupId>org.apache.maven.plugins</groupId>
                   <artifactId>maven-compiler-plugin</artifactId>
                   <version>3.8.1</version>
                   <configuration>
                       <source>12</source>
                       <target>12</target>
                   </configuration>
               </plugin>
           </plugins>
       </build>
   </project>
   ```

4. 在WEB-INF中新建jsp文件夹，在文件夹新建hello.jsp

   ```html
   <%@ page contentType="text/html;charset=UTF-8" language="java" %>
   <html>
   <head>
       <title>Title</title>
   </head>
   <body>
    ${msg}
   </body>
   </html>
   ```

5. HelloController

   ```java
   package com.kuang.controller;
   
   import org.springframework.web.servlet.ModelAndView;
   import org.springframework.web.servlet.mvc.Controller;
   
   import javax.servlet.http.HttpServletRequest;
   import javax.servlet.http.HttpServletResponse;
   
   //导入controller接口
   public class HelloController implements Controller {
       public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
   //        ModelAndView 模型和视图
           ModelAndView mv = new ModelAndView();
   //        封装对象放在modelandview中
           mv.addObject("msg","HelloController");
   //        封装要跳转的视图放在modelandview中
           mv.setViewName("hello");/*     /WEB-INF/jsp/hello.jsp   */
           return mv;
       }
   }
   ```

6. web.xml

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
            version="4.0">
   
       <servlet>
           <servlet-name>springmvc</servlet-name>
           <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
           <init-param>
               <param-name>contextConfigLocation</param-name>
               <param-value>classpath:springmvc-servlet.xml</param-value>
           </init-param>
           <load-on-startup>1</load-on-startup>
       </servlet>
       <servlet-mapping>
           <servlet-name>springmvc</servlet-name>
           <url-pattern>/</url-pattern>
       </servlet-mapping>
   </web-app>
   ```

7. 新建springmvc-servlet.xml

   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans.xsd">
   <!--    添加处理器映射器-->
       <bean class="org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping"></bean>
   
   <!--    添加处理器适配器-->
       <bean class="org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter"></bean>
   
   <!--    添加视图解析器-->
       <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="internalResourceViewResolver">
   <!--        前缀-->
           <property name="prefix" value="/WEB-INF/jsp/"/>
   <!--        后缀-->
           <property name="suffix" value=".jsp"/>
       </bean>
   <!--        配置访问接口的映射地址-->
       <bean id="/hello" class="com.kuang.controller.HelloController"/>
   </beans>
   ```

### 五、第一个springMVC程序（注解版）

1. 在上述的project名为servlet的基础上，新建新的moudel名为springmvc-02将其添加为web项目

2. 新建包com.kuang.controller，在包下新建类HelloController

3. 在resources下新建springmvc-servlet.xml配置文件

4. 在web.xml中注册和配置servlet

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
            version="4.0">
   <!--1、注册servlet-->
       <servlet>
           <servlet-name>springmvc</servlet-name>
           <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
   <!--        通过初始化spring mvc配置文件的位置，进行关联-->
           <init-param>
               <param-name>contextConfigLocation</param-name>
               <param-value>classpath:springmvc-servlet.xml</param-value>
           </init-param>
   <!--        启动顺序，数字越小，启动越早-->
           <load-on-startup>1</load-on-startup>
       </servlet>
   <!--    配置所有请求都会被mvc拦截，/-->
       <servlet-mapping>
           <servlet-name>springmvc</servlet-name>
           <url-pattern>/</url-pattern>
       </servlet-mapping>
   </web-app>
   ```

5. 在springmvc-servlet.xml中配置注解驱动和视图解析器

   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:context="http://www.springframework.org/schema/context"
          xmlns:mvc="http://www.springframework.org/schema/mvc"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans.xsd
          http://www.springframework.org/schema/context
          https://www.springframework.org/schema/context/spring-context.xsd
          http://www.springframework.org/schema/mvc
          https://www.springframework.org/schema/mvc/spring-mvc.xsd">
   <!--    自动扫描包，让指定包下的注解生效，由IOC容器统一管理-->
       <context:component-scan base-package="com.kuang.controller"/>
   <!--    让spring mvc不处理静态资源-->
       <mvc:default-servlet-handler/>
   <!--    支持mvc注解驱动
       在spring mvc中一般采用@RequestMapping注解来完成映射关系
       想要使@RequestMapping生效必须向上下文中注册
       DefaultAnnotationHandlerMapping
       和一个AnnotationMethodHandlerAdapter实例
       这两个实例分别在类级别和方法级别处理。
       而annotation-driven配置帮助我们自动完成上述的两个实例的注入-->
       <mvc:annotation-driven/>
   <!--    视图解析器-->
       <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver"
             id="internalResourceViewResolver">
   <!--        前缀-->
           <property name="prefix" value="/WEB-INF/jsp/"/>
   <!--        后缀-->
           <property name="suffix" value=".jsp"/>
       </bean>
   </beans>
   ```

6. HelloController

   ```java
   package com.kuang.controller;
   
   import org.springframework.stereotype.Controller;
   import org.springframework.ui.Model;
   import org.springframework.web.bind.annotation.RequestMapping;
   
   @Controller
   @RequestMapping("/HelloController")
   public class HelloController {
   //真实访问地址：localhost:8080/HelloController/hello
       @RequestMapping("/hello")
       public String sayHello(Model model){
   //        向模型中添加属性msg与值，可以在jsp页面中取出并渲染
           model.addAttribute("msg","hello,springMvc");
           return "hello";
       }
   }
   ```

   - @Controller是为了让Spring IOC容器初始化时自动扫描到；
   - @RequestMapping是为了映射请求路径，这里因为类与方法上都有映射所以访问时应该是/HelloController/hello；
   - 方法中声明Model类型的参数是为了把Action中的数据带到视图中；
   - 方法返回的结果是视图的名称hello，加上配置文件中的前后缀变成WEB-INF/jsp/**hello**.jsp。

7. 由于maven可能存在资源过滤问题我们在pom.xml上加上资源过滤器

   ```xml
   <build>
      <resources>
          <resource>
              <directory>src/main/java</directory>
              <includes>
                  <include>**/*.properties</include>
                  <include>**/*.xml</include>
              </includes>
              <filtering>false</filtering>
          </resource>
          <resource>
              <directory>src/main/resources</directory>
              <includes>
                  <include>**/*.properties</include>
                  <include>**/*.xml</include>
              </includes>
              <filtering>false</filtering>
          </resource>
      </resources>
   </build>
   ```

8. ***\*/ 和 /\**\**\**\* \**的区别：\****

   **< url-pattern > / </ url-pattern >** 不会匹配到.jsp， 只针对我们编写的请求；即：.jsp 不会进入spring的 DispatcherServlet类 。

   **< url-pattern > /* </ url-pattern >** 会匹配 *.jsp，会出现返回 jsp视图 时再次进入spring的DispatcherServlet 类，导致找不到对应的controller所以报404错。

9. 在WEB-INF中新建jsp文件夹，在文件中新建hello.jsp

   ```html
   <%@ page contentType="text/html;charset=UTF-8" language="java" %>
   <html>
   <head>
      <title>SpringMVC</title>
   </head>
   <body>
   ${msg}
   </body>
   </html>
   ```

### 六、回顾上次的程序（即自己能打出来）

1. 重点，web.xml中要注册servlet和绑定springmvc-servlet.xml配置文件

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
             http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
            version="4.0">
   
       <servlet>
           <servlet-name>springmvc</servlet-name>
           <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
           <init-param>
               <param-name>contextConfigLocation</param-name>
               <param-value>classpath:springmvc-servlet.xml</param-value>
           </init-param>
           <load-on-startup>1</load-on-startup>
       </servlet>
       <servlet-mapping>
           <servlet-name>springmvc</servlet-name>
           <url-pattern>/</url-pattern>
       </servlet-mapping>
   </web-app>
   ```

2. 在springmvc-servlet.xml中配置视图解析器和绑定controller

   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans.xsd">
   
       <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver"
             id="internalResourceViewResolver">
           <property name="prefix" value="/WEB-INF/jsp/"/>
           <property name="suffix" value=".jsp"/>
       </bean>
       <bean name="/t1" class="com.kuang.controller.controller"/>
   </beans>
   ```

3. conntroller类

   ```java
   package com.kuang.controller;
   
   import org.springframework.ui.Model;
   import org.springframework.web.servlet.ModelAndView;
   import org.springframework.web.servlet.mvc.Controller;
   
   import javax.servlet.http.HttpServletRequest;
   import javax.servlet.http.HttpServletResponse;
   
   public class controller implements Controller {
       @Override
       public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
           ModelAndView mv = new ModelAndView();
           mv.addObject("msg","TestController");
           mv.setViewName("test");
           return mv;
       }
   }
   ```

4. 若要使用注解，即在springmvc-servlet.xml里面增加注解扫描

   - 加上<context:component-scan base-package="要扫描的controller包"/>

   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:context="http://www.springframework.org/schema/context"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd">
   
       <context:component-scan base-package="com.kuang.controller"/>
       <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver"
             id="internalResourceViewResolver">
           <property name="prefix" value="/WEB-INF/jsp/"/>
           <property name="suffix" value=".jsp"/>
       </bean>
   </beans>
   ```

5. controller类

   ```java
   package com.kuang.controller;
   
   import org.springframework.stereotype.Controller;
   import org.springframework.ui.Model;
   import org.springframework.web.bind.annotation.RequestMapping;
   import org.springframework.web.servlet.ModelAndView;
   
   import javax.servlet.http.HttpServletRequest;
   import javax.servlet.http.HttpServletResponse;
   @Controller
   public class controller{
   @RequestMapping("/t1")
      public String index(Model mv) {
          mv.addAttribute("msg", "TestController");
          return "test";
      }
   }
   ```

### 七、RestFul 风格

***概念***

Restful就是一个资源定位及资源操作的风格。不是标准也不是协议，只是一种风格。基于这个风格设计的软件可以更简洁，更有层次，更易于实现缓存等机制。

***功能***

资源：互联网所有的事物都可以被抽象为资源

资源操作：使用POST、DELETE、PUT、GET，使用不同方法对资源进行操作。

分别对应 添加、 删除、修改、查询。

***传统方式操作资源*** ：通过不同的参数来实现不同的效果！方法单一，post 和 get

http://127.0.0.1/item/queryItem.action?id=1 查询,GET

http://127.0.0.1/item/saveItem.action 新增,POST

http://127.0.0.1/item/updateItem.action 更新,POST

http://127.0.0.1/item/deleteItem.action?id=1 删除,GET或POST

***使用RESTful操作资源*** ：可以通过不同的请求方式来实现不同的效果！如下：请求地址一样，但是功能可以不同！

http://127.0.0.1/item/1 查询,GET

http://127.0.0.1/item 新增,POST

http://127.0.0.1/item 更新,PUT

http://127.0.0.1/item/1 删除,DELETE

1. 在Spring MVC中可以使用  @PathVariable 注解，让方法参数的值对应绑定到一个URI模板变量上。通过路径变量的类型可以约束访问参数，如果类型不一样，则访问不到对应的请求方法，如这里访问是的路径是/commit/1/a，则路径与方法不匹配，而不会是参数转换失败。

   ```java
   @Controller
   public class RestFulController {
    
       //映射访问路径
       @RequestMapping("/commit/{p1}/{p2}")
       public String index(@PathVariable int p1, @PathVariable int p2, Model model){
           
           int result = p1+p2;
           //Spring MVC会自动实例化一个Model对象用于向视图中传值
           model.addAttribute("msg", "结果："+result);
           //返回视图位置
           return "test";   
       }   
   }
   ```

2. ***使用method属性指定请求类型***

   用于约束请求的类型，可以收窄请求范围。指定请求谓词的类型如GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE, TRACE等

   ```java
   @RequestMapping(value = "/hello",method = {RequestMethod.POST})
   public String index2(Model model){
       model.addAttribute("msg", "hello!");
       return "test";
   }
   ```

3. 方法级别的注解变体有如下几个：组合注解

   ```java
   @GetMapping
   @PostMapping
   @PutMapping
   @DeleteMapping
   @PatchMapping
   ```

   @GetMapping 是一个组合注解，平时使用的会比较多！

   它所扮演的是 @RequestMapping(method =RequestMethod.GET) 的一个快捷方式。

### 八、整合ssm框架

1. 建立数据库ssmbuild，创建表books，在里面插入一些数据

   ```mysql
   USE `ssmbuild`
   
   DROP TABLE IF EXISTS `boooks`;
   
   CREATE TABLE `books`(
   	`bookID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '书id',
   	`bookName` VARCHAR(100) NOT NULL COMMENT '书名',
   	`bookCounts` INT(11) NOT NULL COMMENT '数量',
   	`detail` VARCHAR(200) NOT NULL COMMENT '描述',
   	KEY `bookID`(`bookID`)
   )ENGINE=INNODB DEFAULT CHARSET=utf8
   
   INSERT INTO `books`(`bookID`,`bookName`,`bookCounts`,`detail`) VALUES
   (1,'java',1,'从入门到放弃'),
   (2,'mysql',10,'从删库到跑路'),
   (3,'linux',5,'从入门到进牢');
   ```

2. 搭建maven环境，在先前有junit，spring-mvc，servlet-api，jsp，jtsl的情况下增加配置

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <project xmlns="http://maven.apache.org/POM/4.0.0"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
       <parent>
           <artifactId>servlet</artifactId>
           <groupId>org.example</groupId>
           <version>1.0-SNAPSHOT</version>
       </parent>
       <modelVersion>4.0.0</modelVersion>
   
       <artifactId>springmvc-04</artifactId>
   
       <dependencies>
   <!--        数据库驱动-->
           <dependency>
               <groupId>mysql</groupId>
               <artifactId>mysql-connector-java</artifactId>
               <version>8.0.19</version>
           </dependency>
   <!--        数据库连接池-->
           <dependency>
               <groupId>com.mchange</groupId>
               <artifactId>c3p0</artifactId>
               <version>0.9.5.5</version>
           </dependency>
   <!--        mybatis驱动-->
           <dependency>
               <groupId>org.mybatis</groupId>
               <artifactId>mybatis</artifactId>
               <version>3.5.3</version>
           </dependency>
   <!--        mybatis-spring-->
           <dependency>
               <groupId>org.mybatis</groupId>
               <artifactId>mybatis-spring</artifactId>
               <version>2.0.3</version>
           </dependency>
   <!--        spring-jdbc-->
           <dependency>
               <groupId>org.springframework</groupId>
               <artifactId>spring-jdbc</artifactId>
               <version>5.2.7.RELEASE</version>
           </dependency>
       </dependencies>
   
   </project>
   ```

3. maven资源过滤问题

   ```xml
   <build>
       <resources>
           <resource>
               <directory>src/main/java</directory>
               <includes>
                   <include>**/*.properties</include>
                   <include>**/*.xml</include>
               </includes>
               <filtering>false</filtering>
           </resource>
           <resource>
               <directory>src/main/resources</directory>
               <includes>
                   <include>**/*.properties</include>
                   <include>**/*.xml</include>
               </includes>
               <filtering>false</filtering>
           </resource>
       </resources>
   </build>
   ```

   

