### 一、什么是mybatis

**MyBatis 是一款优秀的持久层框架，它支持自定义 SQL、存储过程以及高级映射。MyBatis 免除了几乎所有的 JDBC 代码以及设置参数和获取结果集的工作。MyBatis 可以通过简单的 XML 或注解来配置和映射原始类型、接口和 Java POJO（Plain Old Java Objects，普通老式 Java 对象）为数据库中的记录。**

### 二、需要的依赖。

1. 首先一个springboot项目想要可以接口访问的话都应该添加一个spring-boot-starter-web的依赖，此依赖为spring-boot提供web服务
2. 其次如果要使用mybatis的话，要添加mybatis-spring-boot-starter依赖，如果不是spring-boot写在前面的话，都是非官方出的依赖，但也不代表着不好用，只是不是springboot官方出的罢了
3. 如果要连MySQL接数据库那么还要添加mysql-connector-java的依赖
4. 而连接数据库的驱动有很多，jdbc就是其中的比较广为使用的一种，所以要添加spring-boot-starter-jdbc依赖，起到底层操作数据库的作用。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>study-springboot</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>2.3.4.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>2.1.1</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.21</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <version>2.3.4.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jdbc</artifactId>
            <version>2.3.4.RELEASE</version>
        </dependency>
    </dependencies>
    <build>
        <resources>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.xml</include>
                </includes>
                <filtering>true</filtering>
            </resource>
        </resources>
    </build>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>springboot-mabaties</artifactId>


</project>
```

### 三、配置数据库连接参数和数据库表的建立

1. 在application.yaml中编写一下参数

   ```yaml
   server:
     port: 8080
   spring:
     datasource:
       driver-class-name: com.mysql.cj.jdbc.Driver
       url: jdbc:mysql://localhost:3306/student?userUnicode=true&charcaterEncoding=UTF-8&serverTimezone=UTC
       username: root
       password: root
   mybatis:
     mapper-locations: classpath*:mapper/*.xml
   ```

2. 在数据库中创建数据库名为studen，数据表名为User的操作

   在User表中添加三个字段，分别为userid、username、password，

   数据类型(数据大小)分别为int(16),varchar(255),varchar(255)

   可设置主键也可以不设置主键，下面是设置主键。

   ![image-20201016111218899](https://i.loli.net/2020/12/30/o4ltvndiGDj2HIQ.png)

3. 在application的同级目录下创建pojo.User.class

   ```java
   package clsld.pojo;
   
   public class User {
       private int userid;
       private String username;
       private String password;
   
       public int getUserid() {
           return userid;
       }
   
       public void setUserid(int userid) {
           this.userid = userid;
       }
   
       public String getUsername() {
           return username;
       }
   
       public void setUsername(String username) {
           this.username = username;
       }
   
       public String getPassword() {
           return password;
       }
   
       public void setPassword(String password) {
           this.password = password;
       }
   
       @Override
       public String toString() {
           return "User{" +
                   "userid=" + userid +
                   ", username='" + username + '\'' +
                   ", password='" + password + '\'' +
                   '}';
       }
   }
   
   ```

### 四、创建UserMapper接口和UserMapper.xml文件

1. UserMapper.interface中的方法与UserMapper.xml的id进行关联，通过在UserMapper.xml下编写对应id的sql语句从而实现接口对数据库操作的调用。

2. 如果是编写mapper.xml文件，要在<xml version="1.0" encoding="utf-8" ?>下面在插入一句<!DOCTYPE mapper PUBLIC "-//mybatis.org/DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">这是将mapper格式的标签引入，既要先声明才有效和有提示。

3. UserMapper.interface

   ```java
   package clsld.mapper;
   
   import clsld.pojo.User;
   import org.apache.ibatis.annotations.Mapper;
   
   import java.util.List;
   @Mapper
   public interface UserMapper {
       public List<User> findalluser();
       public  List<User> findUserByid(int userid);
   }
   
   ```

4. UserMapper.xml放在resource目录下新建的Mapper目录下，因为在application.xml中，我们规定了mybatis会去resource目录下去寻找有.xml的文件并自动读取装配。这里classpath:和classpath*:我在下面做以描述和区别：

   ```java
   classpath：
   只会到你的class路径中查找找文件；
   有多个classpath路径，并同时加载多个classpath路径的情况下，只会从第一个classpath中加载。
       
   classpath*：
   不仅包含class路径，还包括jar文件中（class路径）进行查找；
   有多个classpath路径，并同时加载多个classpath路径的情况下，会从所有的classpath中加载；
   用classpath*:需要遍历所有的classpath，所以加载速度是很慢的；因此，在规划的时候，应该尽可能规划好资源文件所在的路径，尽量避免使用classpath*。
   ```

   如果把UserMapper.xml放在和UserMapper.interface的同级目录下的时候，spring boot在执行查询的时候就会报错，即无法找到interfac对应的文件进行操作数据库操作。

   解决办法也是有的，即在pom.xml文件中添加下面这段配置，意思是让springboot将src/mian/java这个文件夹当作配置资源文件夹，并将文件夹下面的.properties和.xml文件单作配置文件来用。

   ```xml
   <build>
           <resources>
               <resource>
                   <directory>src/main/resources</directory>
                   <includes>
                       <include>**/*.properties</include>
                       <include>**/*.xml</include>
                   </includes>
                   <filtering>true</filtering>
               </resource>
               <resource>
                   <directory>src/main/java</directory>
                   <includes>
                       <include>**/*.properties</include>
                       <include>**/*.xml</include>
                   </includes>
                   <filtering>true</filtering>
               </resource>
           </resources>
       </build>
   ```

### 五、创建Service层， 将接口实例为方法供controller层调用

1. 创建Service.Userservice.class文件

   ```java
   package clsld.Service;
   
   import clsld.mapper.UserMapper;
   import clsld.pojo.User;
   import org.springframework.beans.factory.annotation.Autowired;
   import org.springframework.stereotype.Service;
   
   import java.util.List;
   
   @Service
   public class UserService {
       @Autowired(required = false)
       public UserMapper userMapper;
       public List<User> findalluser(){
           return userMapper.findalluser();
       }
       public List<User> findUserByid(int userid){
           return userMapper.findUserByid(userid);
       }
   }
   
   ```

### 六、创建Controller.UserController.class

```java
package clsld.Controller;

import clsld.Service.UserService;
import clsld.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/all")
    public List<User> getAllUser(){
        return userService.findalluser();
    }
    @RequestMapping("select")
    public List<User> selectUser(@RequestParam int userid){
        return userService.findUserByid(userid);
    }
}

```

```http
###得到所有的用户
GET http://localhost:8080/user/all

###通过userid得到指定用户
GET http://localhost:8080/user/select?userid=1
```



