## spring boot

### 一、数据库连接和JDBCTemplate

1. 在pom.xml中引入jar包

   ```xml
   <!--        web-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-web</artifactId>
               <version>2.3.3.RELEASE</version>
           </dependency>
   <!--        jdbc连接数据库-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-jdbc</artifactId>
               <version>2.3.1.RELEASE</version>
           </dependency>
           <dependency>
               <groupId>mysql</groupId>
               <artifactId>mysql-connector-java</artifactId>
               <version>8.0.19</version>
           </dependency>
   
   ```

2. 在application.properties中写入数据库连接参数

   ```properties
   spring.datasource.username=root
   spring.datasource.password=root
   spring.datasource.url=jdbc:mysql://localhost:3306/clsld?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8
   spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
   ```

3. 测试连接属性

   ```java
   package com.clsld;
   
   import org.junit.jupiter.api.Test;
   import org.springframework.beans.factory.annotation.Autowired;
   import org.springframework.boot.test.context.SpringBootTest;
   
   import javax.sql.DataSource;
   import java.sql.Connection;
   import java.sql.SQLException;
   
   @SpringBootTest
   class ApplicationTests {
       @Autowired
       DataSource dataSource;
       @Test
       void contextLoads() throws SQLException {
           System.out.println(dataSource.getClass());
           Connection connection =dataSource.getConnection();
           System.out.println(connection);
           connection.close();
       }
   
   }
   ```

4. 编写接口代码

   ```java
   package com.clsld.controller;
   
   import org.springframework.beans.factory.annotation.Autowired;
   import org.springframework.jdbc.core.JdbcTemplate;
   import org.springframework.web.bind.annotation.Mapping;
   import org.springframework.web.bind.annotation.RestController;
   
   import java.util.List;
   import java.util.Map;
   
   @RestController
   public class jdbcController {
       @Autowired
       JdbcTemplate jdbcTemplate;
       public List<Map<String,Object>> userList(){
           String sql = "select * from table";
           List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
           return list;
       }
   }
   ```

   

### 二、整合Druid

1. **com.alibaba.druid.pool.DruidDataSource 基本配置参数如下：**

   ![img](https://i.loli.net/2020/12/30/kt6RXdre8jaxIJh.jpg)![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tbWJpei5xcGljLmNuL21tYml6X3BuZy91SkRBVUtyR0M3TFlMaWNPSFZHbnd1N2liR3Zid1hpYllldWJpY2lhd1RkejB0ZzFFS0RqWjF4YUlnalJXOUNaNEFwcjRodk56M2lhUVZRSUtTM3NYeTYyOUxnZy82NDA?x-oss-process=image/format,png)![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tbWJpei5xcGljLmNuL21tYml6X3BuZy91SkRBVUtyR0M3TFlMaWNPSFZHbnd1N2liR3Zid1hpYllldWFWRDZtSzNMSnJ0WjRCNmZSS0NMRGdZaWNBVkd6VFVUeldkQ05CNWxGNHRMcGJjQ1QwdXExRUEvNjQw?x-oss-process=image/format,png)

2. 配置数据源

   

### 扩展在spring boot中扩展spring MVC

```java
package com.clsld.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Locale;

@Configuration
public class MyMvcConfig implements WebMvcConfigurer {
////    public interface ViewResolve 实现了视图解析器接口的类，我们就可以把它看做视图解析器
//
//    @Bean
//    public ViewResolver myViewResolver(){
//        return new MyViewResolver();
//
//    }    //自定义了一个自己的视图解析器MyViewResolver
//    public static class MyViewResolver implements ViewResolver{
//    @Override
//    public View resolveViewName(String s, Locale locale) throws Exception {
//        return null;
//    }
//}
//视图跳转
    //把视图解析接口/clsld定向为/test的接口
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/clsld").setViewName("test");
    }
}

```

