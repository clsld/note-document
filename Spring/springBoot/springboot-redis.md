### Springboot整合redis

#### 一、在springboot2.x之后，原来使用的jedis被替换为lettuce

**jedis:采用的直连，多个线程操作的话，是不安全的，如果想要避免不安全的，使用jedis pool连接池更像BIO模式**

**letture:采用netty，实例可以在多个线程中进行共享，不存在线程不安全的情况！可以减少线程数据，更像NIO模式**

#### 二、springboot-redis配置类的分析

1. spring boot所有的配置类都有一个自动配置类	
   - 以redis为例就是RedisAutoConfiguration
2. 自动配置类都会绑定一个properties文件 
   - Configuration绑定有底层操作的RedisProperties.class
3. 配置文件绑定application.properties

源码分析：

```java
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(RedisOperations.class)
@EnableConfigurationProperties(RedisProperties.class)
@Import({ LettuceConnectionConfiguration.class, JedisConnectionConfiguration.class })
public class RedisAutoConfiguration {

	@Bean
	@ConditionalOnMissingBean(name = "redisTemplate")//如果我们自己定义一个redisTemplate，那么就不会走下面的配置类！
	public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory)
			throws UnknownHostException {
        //默认的RedisTemplate是没有过多设置的，redis是需要序列化对象的。
        //两个泛型都是object，object的类型，我们后使用需要强制转换<String,object>
		RedisTemplate<Object, Object> template = new RedisTemplate<>();
		template.setConnectionFactory(redisConnectionFactory);
		return template;
    }
    
	@Bean
	@ConditionalOnMissingBean
	public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory)
			throws UnknownHostException {
		StringRedisTemplate template = new StringRedisTemplate();
		template.setConnectionFactory(redisConnectionFactory);
		return template;
	}
}
```

#### 三、上手测试

1. 导入依赖

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
   	<modelVersion>4.0.0</modelVersion>
   	<parent>
   		<groupId>org.springframework.boot</groupId>
   		<artifactId>spring-boot-starter-parent</artifactId>
   		<version>2.4.0-SNAPSHOT</version>
   		<relativePath/> <!-- lookup parent from repository -->
   	</parent>
   	<groupId>com.clsld</groupId>
   	<artifactId>demo</artifactId>
   	<version>0.0.1-SNAPSHOT</version>
   	<name>test-redis</name>
   	<description>Demo project for Spring Boot</description>
   
   	<properties>
   		<java.version>1.8</java.version>
   	</properties>
   
   	<dependencies>
   		<dependency>
   			<groupId>org.springframework.boot</groupId>
   			<artifactId>spring-boot-starter-data-redis</artifactId>
   		</dependency>
   		<dependency>
   			<groupId>org.springframework.boot</groupId>
   			<artifactId>spring-boot-starter-web</artifactId>
   		</dependency>
   
   		<dependency>
   			<groupId>org.springframework.boot</groupId>
   			<artifactId>spring-boot-devtools</artifactId>
   			<scope>runtime</scope>
   			<optional>true</optional>
   		</dependency>
   		<dependency>
   			<groupId>org.springframework.boot</groupId>
   			<artifactId>spring-boot-configuration-processor</artifactId>
   			<optional>true</optional>
   		</dependency>
   		<dependency>
   			<groupId>org.projectlombok</groupId>
   			<artifactId>lombok</artifactId>
   			<optional>true</optional>
   		</dependency>
   		<dependency>
   			<groupId>org.springframework.boot</groupId>
   			<artifactId>spring-boot-starter-test</artifactId>
   			<scope>test</scope>
   		</dependency>
           <dependency>
               <groupId>org.springframework.data</groupId>
               <artifactId>spring-data-redis</artifactId>
           </dependency>
   		<dependency>
   			<groupId>org.springframework.data</groupId>
   			<artifactId>spring-data-redis</artifactId>
   		</dependency>
   	</dependencies>
   
   	<build>
   		<plugins>
   			<plugin>
   				<groupId>org.springframework.boot</groupId>
   				<artifactId>spring-boot-maven-plugin</artifactId>
   			</plugin>
   		</plugins>
   	</build>
   
   	<repositories>
   		<repository>
   			<id>spring-milestones</id>
   			<name>Spring Milestones</name>
   			<url>https://repo.spring.io/milestone</url>
   		</repository>
   		<repository>
   			<id>spring-snapshots</id>
   			<name>Spring Snapshots</name>
   			<url>https://repo.spring.io/snapshot</url>
   			<snapshots>
   				<enabled>true</enabled>
   			</snapshots>
   		</repository>
   	</repositories>
   	<pluginRepositories>
   		<pluginRepository>
   			<id>spring-milestones</id>
   			<name>Spring Milestones</name>
   			<url>https://repo.spring.io/milestone</url>
   		</pluginRepository>
   		<pluginRepository>
   			<id>spring-snapshots</id>
   			<name>Spring Snapshots</name>
   			<url>https://repo.spring.io/snapshot</url>
   			<snapshots>
   				<enabled>true</enabled>
   			</snapshots>
   		</pluginRepository>
   	</pluginRepositories>
   
   </project>
   
   ```

   

2. 配置连接

   ```properties
   spring.redis.host=192.168.78.3
   spring.redis.password=110120119
   spring.redis.database=1
   spring.redis.port=6379
   ```

3. 测试

   ```java
   package com.clsld;
   
   import com.clsld.pojo.User;
   import com.fasterxml.jackson.core.JsonProcessingException;
   import com.fasterxml.jackson.databind.ObjectMapper;
   import org.junit.jupiter.api.Test;
   import org.springframework.beans.factory.annotation.Autowired;
   import org.springframework.beans.factory.annotation.Qualifier;
   import org.springframework.boot.test.context.SpringBootTest;
   import org.springframework.data.redis.core.RedisTemplate;
   
   @SpringBootTest
   class TestRedisApplicationTests {
   	@Autowired
   	@Qualifier("redisTemplate")
   	RedisTemplate redisTemplate;
   
   	@Test
   	void contextLoads() {
   		redisTemplate.opsForValue().set("mykeys","myvalues");
   		System.out.println(redisTemplate.opsForValue().get("mykeys"));
   	}
   	@Test
   	public void test() throws JsonProcessingException {
   //		其实真实开发都通过json来传递对象
   		User user = new User("clsld",3);
   		String jsonUser = new ObjectMapper().writeValueAsString(user);
   		redisTemplate.opsForValue().set("user",jsonUser);
   		System.out.println(redisTemplate.opsForValue().get("user"));
   
   	}
   }
   
   ```

   #### 四、手写配置类

   ```java
   package com.clsld.config;
   
   import com.fasterxml.jackson.annotation.JsonAutoDetect;
   import com.fasterxml.jackson.annotation.PropertyAccessor;
   import com.fasterxml.jackson.databind.ObjectMapper;
   import org.springframework.context.annotation.Bean;
   import org.springframework.context.annotation.Configuration;
   import org.springframework.data.redis.connection.RedisConnectionFactory;
   import org.springframework.data.redis.core.RedisTemplate;
   import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
   import org.springframework.data.redis.serializer.StringRedisSerializer;
   
   import java.io.ObjectStreamClass;
   
   @Configuration
   public class RedisConfig {
   //    编写我们自己的RedisTemplate,即自己定义了一个redsiTemplate
       @Bean
       @SuppressWarnings("all")
       public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory redisConnectionFactory){
           RedisTemplate<String, Object> template = new RedisTemplate<String,Object>();
           template.setConnectionFactory(redisConnectionFactory);
   //        jackson序列化配置
           Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
           ObjectMapper om = new ObjectMapper();
           om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
           om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
           jackson2JsonRedisSerializer.setObjectMapper(om);
   //        string的序列化
           StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
   //        key采用String的序列化方式
           template.setKeySerializer(stringRedisSerializer);
   //        hash的key采用string的序列化方式
           template.setHashKeySerializer(stringRedisSerializer);
   //        value序列化采用jackson的方式
           template.setValueSerializer(jackson2JsonRedisSerializer);
   //        hash的values采用jackson
           template.setHashValueSerializer(jackson2JsonRedisSerializer);
           template.afterPropertiesSet();
           return template;
   
       }
   }
   
   ```

   #### 五、手写redis工具类

   ```java
   package com.clsld.utils;
   
   import org.springframework.beans.factory.annotation.Autowired;
   import org.springframework.data.redis.core.RedisTemplate;
   import org.springframework.stereotype.Component;
   import org.springframework.util.CollectionUtils;
   
   import java.util.List;
   import java.util.Map;
   import java.util.Set;
   import java.util.concurrent.TimeUnit;
   
   @Component
   public final class RedisUtils {
       @Autowired
       private RedisTemplate<String, Object> redisTemplate;
   /*
    *指定缓存的失效时间
    *@param key 键
    *@param time 时间 秒
    */
       public boolean expire(String key, long time) {
           try {
               if (time > 0) {
                   redisTemplate.expire(key, time, TimeUnit.SECONDS);
               }
               return true;
           }catch (Exception e){
               e.printStackTrace();
               return false;
           }
       }
       /*
       * 根据key获取过期时间
       * @param key 键 不能为null
       * @return 时间（秒） 返回0代表为永久有效
       * */
       public long getExpire(String key){
           return redisTemplate.getExpire(key,TimeUnit.SECONDS);
       }
       /*
       * 判断key是否存在
       * @param key 键
       * @return true 存在 false 不存在
       * */
       public boolean hasKey(String key){
           try {
               return redisTemplate.hasKey(key);
           }catch (Exception e){
               e.printStackTrace();
               return false;
           }
       }
   
       /*
       * 删除缓存
       * @param key 可以传一个值 或多个
       * */
       public void del(String... key){
           if(key != null && key.length > 0){
               if(key.length == 0){
                   redisTemplate.delete(key[0]);
               }else {
                   redisTemplate.delete(String.valueOf(CollectionUtils.arrayToList(key)));
               }
           }
       }
       /*
       * 普通缓存获取
       * */
       public Object get(String key){
           return key == null ? null : redisTemplate.opsForValue().get(key);
       }
       /*
       * 普通缓存放入
       * */
       public Boolean set(String key,Object value){
           try {
               redisTemplate.opsForValue().set(key, value);return true;
           }catch (Exception e) {
               e.printStackTrace();return false;
           }
       }
   
       /*
       * 普通缓存放入并设置时间
       * */
       public boolean set(String key, Object value, long time) {
           try {
               if (time > 0) {
                   redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
               }else {
                   set(key, value);
               }
               return true;
           }catch (Exception e) {
               e.printStackTrace();
               return false;
           }
       }
       /*
       * 递增
       * @param key
       * @param dalta 要增加几（大于几）
       * */
       public long incr(String key, long delta) {
           if (delta < 0) {
               throw new RuntimeException("递增因子必须大于0");
           }
           return redisTemplate.opsForValue().increment(key, delta);
       }
       /*
        * 递减
        * @param key
        * @param dalta 要减少几（小于几）
        * */
       public long decr(String key, long delta) {
           if (delta < 0) {
               throw new RuntimeException("递减因子必须大于0");
           }
           return redisTemplate.opsForValue().increment(key, -delta);
       }
       /*
       * HashGet
       * @param key 键 不能为null
       * @param item 项 不呢为null
       * */
       public Object hget(String key, String item) {
           return redisTemplate.opsForHash().get(key, item);
       }
       /*
       * 获取hashkey对应的所有值
       * @param key 键
       * @return 对应的多个键值对
       * */
       public Map<Object, Object> hmget(String key) {
           return redisTemplate.opsForHash().entries(key);
       }
       /*
       * hashSet
       * @param key
       * @param map 对应多个键值
       * */
       public  boolean hmset(String key, Map<String, Object> map) {
           try {
               redisTemplate.opsForHash().putAll(key, map);
               return true;
           }catch (Exception e){
               e.printStackTrace();
               return false;
           }
       }
       /*
       *同一张hash表中放入数据，如果不存你在则创建
       * @param key
       * @param item
       * @param value
       * @return true 成功 false 失败
       * */
       public boolean hset(String key, String item, Object value) {
           try {
               redisTemplate.opsForHash().put(key, item, value);
               return true;
           }catch (Exception e){
               e.printStackTrace();
               return false;
           }
       }
   
       /*
       * 同一张hash表中放入数据，如果不存在则创建
       * @param key
       * @param item
       * @param value
       * @param time 时间，如果已存在hash表有时间，这里会将原有时间替换
       * @return
       * */
       public boolean hset(String key, String item, Object value, long time) {
           try {
               redisTemplate.opsForHash().put(key, item, value);
               if (time > 0) {
                   expire(key, time);
               }return true;
           }catch (Exception e) {
               e.printStackTrace();return false;
           }
       }
       /*
        * 删除hash表中的值
        */
       public void hdel(String key, Object ... item) {
           redisTemplate.opsForHash().delete(key, item);
       }
       /*
       * 判断hash表中的值
       *
       * */
       public boolean hHasKey(String key, Object value) {
           return redisTemplate.opsForHash().hasKey(key, value);
       }
   //    hash递增 如果不存在，就创建一个，并把新增后的值返回
       public double hincr(String key,String item,double by){
           return redisTemplate.opsForHash().increment(key, item, by);
       }
   //    hash递减
       public double hdecr(String key, String item, double by) {
           return redisTemplate.opsForHash().increment(key, item, -by);
       }
   //    根据key获取set中的所有值
       public Set<Object> sGet(String key) {
           try {
               return redisTemplate.opsForSet().members(key);
           }catch (Exception e){
               e.printStackTrace();
               return null;
           }
       }
   //    根据value从set中查询，是否存在
       public boolean sHasKey(String key, Object value) {
           try {
               return redisTemplate.opsForSet().isMember(key, value);
           }catch (Exception e) {
               e.printStackTrace();
               return false;
           }
       }
   //    将数据放入set缓存
       public long sSet(String key, Object ... values){
           try {
               return redisTemplate.opsForSet().add(key, values);
           }catch (Exception e){
               e.printStackTrace();
               return 0;
           }
       }
   //    将set数据放入缓存
       public long sSetAndTime(String key, long time, Object ... values){
           try {
               Long count = redisTemplate.opsForSet().add(key, time, values);
               if (time > 0) {
                   expire(key, time);
               }
               return count;
           }catch (Exception e){
               e.printStackTrace();
               return 0;
           }
       }
   //    获取set缓存的长度
       public long sGetSetSize(String key) {
           try {
               return redisTemplate.opsForSet().size(key);
           }catch (Exception e){
               e.printStackTrace();
               return 0;
           }
       }
   //    移除值为values的set
       public long setRemove(String key, Object ... value){
           try {
               Long count = redisTemplate.opsForSet().remove(key, value);
               return count;
           }catch (Exception e) {
               e.printStackTrace();
               return 0;
           }
       }
   //    获取list缓存的内容
       public List<Object> lGet(String key, long start, long end) {
           try {
               return redisTemplate.opsForList().range(key, start, end);
           }catch (Exception e) {
               e.printStackTrace();
               return null;
           }
       }
   //    获取list缓存的长度
       public long lGetListSize(String key) {
           try {
               return redisTemplate.opsForList().size(key);
           }catch (Exception e) {
               e.printStackTrace();
               return 0;
           }
       }
   //    通过索引获取list中的值
       public Object lGetIndex(String key, long index) {
           try {
               return redisTemplate.opsForList().index(key, index);
           }catch (Exception e){
               e.printStackTrace();
               return null;
           }
       }
   //    将list放入缓存
       public boolean lSet(String key, Object value,long time) {
           try {
               redisTemplate.opsForList().rightPush(key, value);
               if (time > 0) {
                   expire(key, time);
               }
               return true;
           }catch (Exception e) {
               e.printStackTrace();
               return false;
           }
       }
   //    将list放入缓存
       public boolean lSet(String key,List<Object> value) {
           try {
               redisTemplate.opsForList().rightPushAll(key, value);
               return true;
           }catch (Exception e) {
               e.printStackTrace();
               return false;
           }
       }
   //    根据索引修改list中的某条数据
       public boolean lUpdateIndex(String key, long index, Object value) {
           try {
               redisTemplate.opsForList().set(key, index, value);
               return true;
           }catch (Exception e) {
               e.printStackTrace();
               return false;
           }
       }
   //    移除n个值为value
       public long lRemove(String key, long count, Object value) {
           try {
               Long remove = redisTemplate.opsForList().remove(key, count,value);
               return remove;
           }catch (Exception e) {
               e.printStackTrace();
               return 0;
           }
       }
   }
   ```

   