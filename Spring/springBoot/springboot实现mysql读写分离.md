## SpringBoot与mysql读写分离

实现方案：

> 使用中间件（mycat）、代码层面进行读写分离

## 代码层面进行读写分离

### 基于spring提供的原生的AbstractRoutingDataSource搭配AOP进行实现

`springboot+mybatis+druib连接池。想要实现读写分离就需要配置多个数据源，在进行写操作时选择写的数据源，在进行读操作时选择读的数据源，其中有两个关键点：如何切换数据源、如何根据不同的方法选择正确的数据源`

#### 如何切换数据源

通常用springboot时都是使用它的默认配置，只需要在配置文件中定义好连接属性就行了，但是现在我们需要自己来配置了，spring是支持多数数据源的，多个DataSource放在同一个hashmap **TargetDataSource**中，通过**determineCurrentLookupKey**获取key来决定要使用哪个数据源，因此我们的目标就很明确了，建立多个DataSource放到**TargetDataSource**中，同时重写**determineCurrentLookupKey**方法来决定使用哪个key。

#### 如何选择数据源

事务一般是注解在Service层的，因此在开始这个service方法调用是要确定数据源，那有什么通用的方法能够在开始执行一个方法前做操作呢？其实就是运用了AOP。`可以定义一个只读注解，被该数据标注的方法使用读库，也通过方法名，根据方法名写切点，比如getXXX用读库，setXXX用写库。`

##### 代码编写

编写配置文件，配置两个数据源的信息，参考如下：

```yaml
mysql:
	datasource:
	#读库数目
	num: 1
	type-alises-package: com.clsld.dao
	mapper-locations: classpath:/mapper/*.xml
	config-location: classpath:/mybatis-config.xml
	write:
		url: jdbc:mysql://192.168.226.5:3306/test?useUnicode=true&characterEncoding=utf-8&useSSL=true
		username: root
		password: root
		driver-class-name: com.mysql.jdbc.Driver
	read:
		url: jdbc:mysql://192.168.226.6:3306/test?useUnicode=true&characterEncoding=utf-8&useSSL=true
		username: root
		password: root
		driver-class-name: com.mysql.jdbc,Driver

###### 编写DbContextHolder类

这个类用来设置数据库类别，其中有一个ThreadLocal用来保存每个线程使用的是读库还是写库

```java
public class DbContextHolder{
    private static Logger log = LoggerFactory.getLogger(DbContextHolder.class);
    public static final String WRITE = "write";
    public static final String READ = "read";
    private static ThreadLocal<String> contextHolder = new ThreadLocal<>();
    public static void setDbType(String dbType){
        if(dbType == null){
            log.error("dbType为空");
            throw new NullPointerException();
        }
        log.info("设置dbType为:{}",dbType);
        contextHolder.set(dbType);
    }
    public static String getDbType(){
        return contextHolder.get() == null? WRITE : contextHolder.get();
    }
    public static void clearDbType(){
        contextHolder.remove();
    }
}
```

###### 重写determineCurrentLookupKey方法

Spring在开始进行数据库操作是会通过这个方法来决定使用哪个数据库，因此我们在这里调用上面DbContextHolder类的getDbType()方法获取当前操作类别，同时可进行读库的负载均衡代码如下：

```java
public class MyAbstractRoutingDataSource extends AbstractRoutingDataSource{
    @Value("${mysql.datasource.num}")
    private int num;
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @Override
    protected Object determinCurrentLookupKey(){
        String typeKey = DbContextHolder.getDbType();
        if(typeKey == DbContextHolder.WRITE){
            log.info("使用了写库");
            return typeKey;
        }
        //使用随机数决定使用哪个读库
        int sum = NumberUtil.getRandom(1,num);
        log.info("使用了读库{}",sum);
        return DbContextHolder.READ + sum;
    }
}
```

###### 编写配置类

由于要进行读写分离，不能再用springboot的默认配置，我们需要手动来进行配置，首先生成数据源，使用@ConfigurProperties自动生成数据源：

```java
@Primary
@Bean
@ConfigurationProperties(prefix = "mysql.datasource.write")
public DataSource writeDataSource(){
    return new DruidDataSource();
}
```

读数据源类似，注意有多少个读库就要设置多少个度数据源，Bean名为read+序号。

###### 然后设置数据源

使用的是我们之前写的MyAbstractRoutingDataSource类

```java
//设置数据源路由，通过该类中的determineCurrentLookupKey决定使用哪个数据源
@Bean
public AbstractRoutingDataSource routingDataSource(){
    MyAbstractRoutingDataSource proxy = new MyAbstractRoutingDataSource();
    Map<Object,Object> targetDataSources = new HashMap<>();
    targetDataSources.put(DbContextHolder.WRITE,writeDataSource());
    targetDataSources.put(DbContextHolder.READ + "1",read1());
    proxy.setDefaultTargetDataSource(writeDataSource());
    proxy.setTargetDataSources(targetDataSources);
    return proxy;
}
```

###### 接着需要设置sqlSessionFactory

```java
@Bean
public SqlSessionFactory sqlSessionFactory() throws Exception{
    SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
    bean.setDataSource(routingDataSource());
    ResourcePatternResolver resolver = new PathMacthingResourcePatternResolver();
    //实体类对应的位置
    bean.setTypeAliasesPackage(typeAliasesPackage);
    //mybatis的xml配置
    bean.setMapperLocations(resolver.getResources(mapperLocation));
    bean.setConfigLocation(resolver.getResource(configLocation));
    return bean.getObject();
}
```

###### 最后还得配置一下事务，否则事务不生效

```java
@Bean
public DataSourceTransactionManger datasourceTransactionManager(){
    return new DataSourceTransactionManager(routingDataSource());
}
```

#### 选择数据源

`多数据源配置好了，但是代码层怎么选择数据源呢，这里有两种方法`

##### 注解式

首先定义一个只读注解，被这个注解方法使用读库，其他使用写库，如果项目是中途改造成读写分离可使用这个方法，无需修改业务代码，只要在只读的service方法上加一个注解即可

```java
@Target({ELementType.METHOD,ELementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ReadOnly{}
```

然后写一个切面来切换数据使用哪种数据源，重写getOrder保证本切面优先级高于事务切面优先级，在启动类加上@EnableTransactionManagement(order = 10)，代码如下：

```java
@Aspect
@Component
public class ReadOnlyInterceptor implements Orderd{
    private static final Logger log = LoggerFactory.getLogger(ReadOnlyInterceptor.class);
    @Around("@annotation(readOnly)")
    public Object setRead(ProceedingJoinPoint joinPoint,ReadOnly readOnly) throws Throwable{
        try{
            DbContextHolder.setDbType(DbContextHolder.READ);
            return joinPoint.proceed();
        }finally{
            DbContextHolder.clearDbType();
            log.info("清除threadLocal");
        }
    }
}
```

##### 方法名式

`这种方法不需要注解，但是需要service中的方法名按一定规则编写，然后通过切面来设置数据类别，比如setXXX设置为写、getXXX设置为读`

https://mp.weixin.qq.com/s?__biz=MzI1MTY1Njk4NQ==&mid=2247484397&idx=1&sn=79131f11bcb51dbc96a8922474120aa4&chksm=e9eeed9bde99648de30c8b47d566b0029c5676975dd564779e103b4f7add7d0e7bd900e4fd8d&token=1054838354&lang=zh_CN&scene=21#wechat_redirect

### 使用mybatis-plus提供的dynamic-datasource-spring-boot-starter进行实现

```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
</dependency>
```

application.yml配置

```yaml
spring:
  datasource:
    dynamic:
      primary: leader
      datasource:
        leader:
          username: root
          password: 123456
          driver-class-name: com.mysql.cj.jdbc.Driver
          url: jdbc:mysql://localhost:3306/boot-learning?allowMultiQueries=true&useUnicode=true&characterEncoding=UTF-8&useSSL=false&autoReconnect=true&serverTimezone=UTC
          druid: #以下均为默认值
            initial-size: 3
            max-active: 8
            min-idle: 2
            max-wait: -1
            min-evictable-idle-time-millis: 30000
            max-evictable-idle-time-millis: 30000
            time-between-eviction-runs-millis: 0
            validation-query: select 1
            validation-query-timeout: -1
            test-on-borrow: false
            test-on-return: false
            test-while-idle: true
            pool-prepared-statements: true
            max-open-prepared-statements: 100
            filters: stat,wall
            share-prepared-statements: true
        follow:
          username: root
          password: 123456
          driver-class-name: com.mysql.cj.jdbc.Driver
          url: jdbc:mysql://localhost:3306/springboot-learning?allowMultiQueries=true&useUnicode=true&characterEncoding=UTF-8&useSSL=false&autoReconnect=true&serverTimezone=UTC
          druid: #以下均为默认值
            initial-size: 3
            max-active: 8
            min-idle: 2
            max-wait: -1
            min-evictable-idle-time-millis: 30000
            max-evictable-idle-time-millis: 30000
            time-between-eviction-runs-millis: 0
            validation-query: select 1
            validation-query-timeout: -1
            test-on-borrow: false
            test-on-return: false
            test-while-idle: true
            pool-prepared-statements: true
            max-open-prepared-statements: 100
            filters: stat,wall
            share-prepared-statements: true
            #DruidDataSourceAutoConfigure会注入一个DataSourceWrapper，其会在原生的spring.datasource下找url,username,password等。而我们动态数据源的配置路径是变化的。
  autoconfigure:
    exclude: com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure,io.shardingsphere.shardingjdbc.spring.boot.SpringBootConfiguration
```

在service层上加上@DS注解，通过@DS注解指定要操作的数据库

```java
@Override
  @Transactional
  @DS("follow")
  public BookDTO addBook(BookDTO bookDTO) {
    Book book = dozerMapper.map(bookDTO,Book.class);
    boolean isExitBookByName = ObjectUtils.isNotEmpty(getBookByName(bookDTO.getBookName()));
    if(isExitBookByName){
      throw new BizException("书名已经存在");
    }
    book.setCreateDate(new Date());
    book.setUpdateDate(new Date());
    baseMapper.insert(book);

    bookDTO = dozerMapper.map(book,BookDTO.class);

    return bookDTO;
  }
```

### 使用sharding-JDBC进行实现

![image-20210704004407904](C:\Users\clsld\Documents\我的坚果云\Spring\springBoot\image-20210704004407904.png)

```xml
<dependency>
    <groupId>io.shardingsphere</groupId>
    <artifactId>sharding-jdbc-spring-boot-starter</artifactId>
</dependency>
```

application.yaml配置

```yaml
sharding:
  jdbc:
    datasource:
      #Canonical names should be kebab-case ('-' separated), lowercase alpha-numeric characters and must start with a letter
      names: master-ds,slave-ds
      master-ds:
        type: com.alibaba.druid.pool.DruidDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://localhost:3306/boot-learning?allowMultiQueries=true&useUnicode=true&characterEncoding=UTF-8&useSSL=false&autoReconnect=true&serverTimezone=UTC
        username: root
        password: 123456
      slave-ds:
        type: com.alibaba.druid.pool.DruidDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://localhost:3306/springboot-learning?allowMultiQueries=true&useUnicode=true&characterEncoding=UTF-8&useSSL=false&autoReconnect=true&serverTimezone=UTC
        username: root
        password: 123456
    config:
      sharding:
        props:
          sql.show: true
      masterslave:
        load-balance-algorithm-type: round_robin
        name: dataSource
        master-data-source-name: master-ds
        slave-data-source-names: slave-ds
spring:
  main:
    allow-bean-definition-overriding: true
```

