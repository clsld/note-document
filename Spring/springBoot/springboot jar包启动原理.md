## SpringBoot jar包启动原理

springboot提供了maven插件`spring-boot-maven-plugin`，可以方便的将springboot项目打包成jar包或者war包

考虑部署的便利性我们绝大多数情况下都会打成jar包，因为这样我们无需在部署项目的时候配置相应的tomcat、jetty等Servlet容器

jar包的结构图，一共分为四个部分：

![Spring Boot  包](http://www.iocoder.cn/images/Spring-Boot/2019-01-07/01.png)

META-INF目录，通过MANIFEST.MF文件提供jar包的元数据，声明了jar的启动类。

org目录，为springboot提供spring-boot-loader项目，他是java -jar启动springboot项目的密码所在，也是我们需要深入理解的部分

BOOT-INF/lib目录，是springboot项目中引入的依赖的jar包，spring-boot-loader项目很大的一个作用就是解决jar包里面嵌套jar的情况，如何加载到其中的类。

BOOT-INF/classes目录是我们在springboor项目中java类所编译的.class、配置文件等。

spring-boot-loader项目需要解决的两个问题：

第一个是如何引导执行我们创建的springboot应用的启动类，即application类

第二个是如何加载BOOT-INF/class目录下的类，以及BOOT-INF/lib目录下内嵌的jar包中的类



MANIFEST.MF

文件中的内容如下：

```
Manifest-Version: 1.0
Implementation-Title: lab-39-demo
Implementation-Version: 2.2.2.RELEASE
Start-Class: cn.iocoder.springboot.lab39.skywalkingdemo.Application
Spring-Boot-Classes: BOOT-INF/classes/
Spring-Boot-Lib: BOOT-INF/lib/
Build-Jdk-Spec: 1.8
Spring-Boot-Version: 2.2.2.RELEASE
Created-By: Maven Archiver 3.4.0
Main-Class: org.springframework.boot.loader.JarLauncher
```

它实际上是一个properites配置文件，每一行都是一个配置项目，重点来看这两个配置项：

Main-Class配置项：java规定的jar包启动类，这里设置为spring-boot-loader项目的JarLauncher类，进行springboot应用的启动。

Start-Class配置项：springboot规定的主启动类，这里设置为我们定义的application类

为什么会有 `Main-Class`/`Start-Class` 配置项呢？因为我们是通过 Spring Boot 提供的 Maven 插件 [`spring-boot-maven-plugin`](https://github.com/spring-projects/spring-boot/blob/master/spring-boot-project/spring-boot-tools/spring-boot-maven-plugin/) 中，从而能让 `spring-boot-loader` 能够引导启动 Spring Boot 应用。

Start-class对应的Application类自带了#main(String[] args)方法，为什么我们不能直接运行呢?我们来尝试一下在控制台运行

```
$ java -classpath lab-39-demo-2.2.2.RELEASE.jar cn.iocoder.springboot.lab39.skywalkingdemo.Application
错误: 找不到或无法加载主类 cn.iocoder.springboot.lab39.skywalkingdemo.Application

```

直接报错说找不到Application类，因为他在BOOT-INF/class目录下，不符合java默认的jar包的加载规则，因此需要通过JarLaucher启动加载。

当然还有一个重要的原因是，java规定的可执行器的jar包是禁止嵌套其他jar包的，但是我们看到BOOT-INF、lib目录下，实际上有springboot应用依赖的所有jar包，因此spring-boot-loader项目自定义实现了ClassLoader实现类LauncherURLClassLoader，支持加载BOOT-INF/classes目录下的.class文件，以及BOOT-INF、lib目录下的jar包。

JarLauncher



JarLauncher类是针对springboot jar包的启动类，整体的构造如下图所示：

![JarLauncher 类图](http://www.iocoder.cn/images/Spring-Boot/2019-01-07/11.png)

WarLauncher类是针对springboot war包的启动类，

```java
public class JarLauncher extends ExecutableArchiveLauncher {	static final String BOOT_INF_CLASSES = "BOOT-INF/classes/";	static final String BOOT_INF_LIB = "BOOT-INF/lib/";	public JarLauncher() {	}	protected JarLauncher(Archive archive) {		super(archive);	}	@Override	protected boolean isNestedArchive(Archive.Entry entry) {		if (entry.isDirectory()) {			return entry.getName().equals(BOOT_INF_CLASSES);		}		return entry.getName().startsWith(BOOT_INF_LIB);	}	public static void main(String[] args) throws Exception {		new JarLauncher().launch(args);	}}
```