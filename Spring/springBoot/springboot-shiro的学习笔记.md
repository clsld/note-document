# shiro认证的运行过程

在resource下创建shiro.ini,写入一下数据模拟用户用户名密码

```ini
[users]
陈霖 = 110120119
clsld = 123456
```

main函数模拟shiro的认证过程

```java
package com.clsld;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;

public class TestAuthenticator {
    public static void main(String[] args) {
//        创建安全管理器对象
        DefaultSecurityManager securityManager = new DefaultSecurityManager();
//        给安全管理器设置realm
        securityManager.setRealm(new IniRealm("classpath:shiro.ini"));
//        SecurityUtils给全局安全工具类设置安全管理器
        SecurityUtils.setSecurityManager(securityManager);
//        关键对象subject主体
        Subject subject = SecurityUtils.getSubject();
//        创建令牌
        UsernamePasswordToken token = new UsernamePasswordToken("陈霖","110120119");
        try {
//            用户认证
            subject.login(token);
//            判断认证状态
            System.out.println(subject.isAuthenticated());
        }catch (UnknownAccountException e){
            e.printStackTrace();
        }catch (IncorrectCredentialsException e){
            e.printStackTrace();
        }
    }
}

```

最终用户名的校验是在SimpleAccountRealm中的doGetAuthenticationInfo方法中完成

最终密码的校验是在AuthenticatingRealm中assertCredentialsMath方法中完成

# 自定义Realm

创建下面的自定义Realm类

```java
package com.clsld.Realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

/*
* 自定义realm的实现 将认证、授权数据的来源转化为数据库实现
* * */
public class CustomerRealm extends AuthorizingRealm {
//   认证
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }
//   授权

    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
//        在token中获取用户名
        String principal = (String) authenticationToken.getPrincipal();
        System.out.println(principal);
//        根据身份信息使用查询数据库辨认
        if("clsld".equals(principal)){
//            参数1是数据库中的用户名，参数2是数据库中的密码，参数3是提供当前Realm的名字
            SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo("clsld","110120",this.getName());
            return simpleAuthenticationInfo;
        }
        return null;
    }
}
```

编写测试的函数

```java
package com.clsld.Realm;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;


public class TestCustomerRealmAuthenticator {
    public static void main(String[] args) {
//        创建securityManager
        DefaultSecurityManager securityManager = new DefaultSecurityManager();
//        设置自定义realm
        securityManager.setRealm(new CustomerRealm());
//        设置安全工具类
        SecurityUtils.setSecurityManager(securityManager);
//        通过设置安全工具类获取subject
        Subject subject = SecurityUtils.getSubject();
//        创建token
        UsernamePasswordToken token = new UsernamePasswordToken("clsld","110120119");
        try{
            subject.login(token);
        }catch (UnknownAccountException e){
            e.printStackTrace();
            System.out.println("用户名异常");
        }catch (IncorrectCredentialsException e){
            e.printStackTrace();
            System.out.println("密码错误");
        }
    }
}
```

# MD5加密

自定义Realm类

```java
package com.clsld.Realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

public class CudtomerMD5Realm extends AuthorizingRealm {
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
       String principal = (String) authenticationToken.getPrincipal();
       if ("clsld".equals(principal)){
//           MD5加密要在config中开启hashedCredentialsMatcher.setHashAlgorithmName("md5");
            return new SimpleAuthenticationInfo(principal,"asdfasdvavafv",this.getName());
//            MD5加密+盐要增加参数
//            return new SimpleAuthenticationInfo(principal,"asdfasdvavafv",, ByteSource.Util.bytes("x*07"),this.getName());
//            MD5加密+盐+hash散列要在config中开启散列 hashedCredentialsMatcher.setHashIterations(1024);
       }
        return null;
    }
}

```

创建test类

```java
package com.clsld.Realm;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;

public class TestMD5Realmauthenticator {
    public static void main(String[] args) {
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        CudtomerMD5Realm cudtomerMD5Realm = new CudtomerMD5Realm();
//        设置realm使用的hash凭证匹配器
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
//        开启md5加密
        hashedCredentialsMatcher.setHashAlgorithmName("md5");
//        散列
        hashedCredentialsMatcher.setHashIterations(1024);
        cudtomerMD5Realm.setCredentialsMatcher(hashedCredentialsMatcher);
        defaultSecurityManager.setRealm(cudtomerMD5Realm);
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("clsld","110120119");
        try{
            subject.login(token);
        }catch (UnknownAccountException e){
            e.printStackTrace();
        }catch (IncorrectCredentialsException e){
            e.printStackTrace();
        }
    }
}
```

![image-20210316210242735](https://blog-1300924781.cos.ap-guangzhou.myqcloud.com/blog/image-20210316210242735.png)





shiro的SessionManager管理着所有subject的会话创建、维护、删除、失效、验证等工作。SecurityManager直接继承了SessionManager。





我认为我自己在坚定党的理想信念、增强历史自觉这两个方面做得还是不错的，接下来我会继续坚定不移的跟党走，自觉学习党章党史。在弘扬优良传统方面我觉得我还是有很多不足，对于优秀的传统文化没有去学习和弘扬它，接下来这方面也是我努力改善的方向。

其次是习近平在中国共产党100周年的讲话上，我记得有段话让我十分深刻，中国人民也决不允许任何外来势力欺负、压迫、奴隶我们，谁妄想这样干必将在14多亿中国人民用血肉筑成的钢铁长城面前碰得头破血流。这是中国对世界的宣誓，任何人都不要低估中国人民捍卫国家主权和领土完整的坚定决心、坚定意志和强大能力。

最后我积极发挥党员的先锋模范作用，以身作则时刻牢记为人民服务的宗旨，不过我也有做得不好的地方，例如在实践组的工作中没有做到进行尽力，对于志愿活动也没有做到积极参加，这都是我要自我检讨的地方，接下来我会努力改善自己不足的地方。

