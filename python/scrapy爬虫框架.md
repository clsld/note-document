## Scrapy爬虫框架学习笔记

#### 一、scrapy爬虫的简单使用

```python
#创建项目：
scrapy start project xxx
#进入项目,创建爬虫：
scrapy genspider xxx(爬虫名) xxx.com(爬取域)
#生成文件：
scrapy crawl xxx  -o xxx.json
#运行爬虫：
scrapy srawl xxx


```

#### 二、，目录结构解释

**scrapy startproject clsld**

**scrapy.cfg: 项目的配置文件**

**clsld/: 该项目的python模块**

**clsld/items.py: 项目中的item文件.（这是创建容器的地方，爬取的信息分别放到不同容器里）**

**clsld/pipelines.py: 项目中的pipelines文件.**

**clsld/settings.py: 项目的设置文件.（我用到的设置一下基础参数，比如加个文件头，设置一个编码）****

**clsld/spiders/: 放置spider代码的目录. （放爬虫的地方）**