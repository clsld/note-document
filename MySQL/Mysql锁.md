- 共享锁（读锁）：其他事务可以读，但不能写。

- 排他锁（写锁） ：其他事务不能读取，也不能写。

  

MyISAM和Memory存储引擎采用的是表级锁

InnoDB存储引擎支持行级锁，也支持表级锁，默认情况下是采用行级锁

表级锁：开销小，加锁块，不会出现死锁，锁定粒度大，发生锁冲突的概率最高，并发度最低，表级锁更适合用于查询为主，并发用户少，只有少量按索引条件更新数据的应用。

行级锁：开销大，加锁慢，会出现死锁，锁定粒度最小，发生锁冲突的概率最低，并发度最高，行级锁更适合于有大量按索引条件并发更新少量不同数据。

页面锁，开销和加锁时间界于表锁和行锁之间，会出现死锁。



MyIsam表锁模式：

- 表共享读锁：不会阻塞其他用户对同一表的读请求，但会阻塞对同一表的写请求
- 表独占写锁：会阻塞其他用户对同一表的读和写操作

当一个线程获得对一个表的写锁后， 只有持有锁的线程可以对表进行更新操作。 其他线程的读、 写操作都会等待，直到锁被释放为止。默认情况下，写锁比读锁具有更高的优先级：当一个锁释放时，这个锁会优先给写锁队列中等候的获取锁请求，然后再给读锁队列中等候的获取锁请求。

可以设置改变读锁和写锁的优先级：

- 通过指定启动参数low-priority-updates，使MyISAM引擎默认给予读请求以优先的权利。
- 通过执行命令SET LOW_PRIORITY_UPDATES=1，使该连接发出的更新请求优先级降低。
- 通过指定INSERT、UPDATE、DELETE语句的LOW_PRIORITY属性，降低该语句的优先级。
- 给系统参数[max_write_lock_count](https://www.zhihu.com/search?q=max_write_lock_count&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"article"%2C"sourceId"%3A29150809})设置一个合适的值，当一个表的读锁达到这个值后，MySQL就暂时将写请求的优先级降低，给读进程一定获得锁的机会。

MyIsam加表锁的方法：

​	MyISAM 在执行查询语句（SELECT）前，会自动给涉及的表加读锁，在执行更新操作（UPDATE、DELETE、INSERT 等）前，会自动给涉及的表加写锁，这个过程并不需要用户干预，因此，用户一般不需要直接用 LOCK TABLE 命令给 MyISAM 表显式加锁。



InnoDB行锁和表锁

- 共享锁：允许一个事务去读一行，阻止其他事务获得相同数据集的排他锁
- 排他锁：排他锁指的是一个事务在一行数据加上排他锁后，其他事务不能再在其上加其他的锁

