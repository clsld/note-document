### [组合两个表](https://leetcode-cn.com/problems/combine-two-tables/)

> 表1: Person
>
> ```
> +-------------+---------+
> | 列名         | 类型     |
> +-------------+---------+
> | PersonId    | int     |
> | FirstName   | varchar |
> | LastName    | varchar |
> +-------------+---------+
> PersonId 是上表主键
> ```
>
>
> 表2: Address
>
> ```
> +-------------+---------+
> | 列名         | 类型    |
> +-------------+---------+
> | AddressId   | int     |
> | PersonId    | int     |
> | City        | varchar |
> | State       | varchar |
> +-------------+---------+
> AddressId 是上表主键
> ```
>
> 编写一个 SQL 查询，满足条件：无论 person 是否有地址信息，都需要基于上述两表提供 person 的以下信息：
>
> ```
> FirstName, LastName, City, State
> ```

```sql
# 题目信息是无论person是否有地址信息都要以他为依据，就是就算没有地址信息也要打印出null来。如果用inner join的话，Address表如果没有和Person表中相同的personID，没有相同persionID的那部分的地址信息就不会打印出来，所以使用outer join可以避免这种情况
select FirstName,LastName,City,State form Persion left join Address on Person.PersonId = Address.PersonId;
```

### [第二高的薪水](https://leetcode-cn.com/problems/second-highest-salary/)

> SQL架构:
>
> ```sql
> Create table If Not Exists Employee (Id int, Salary int)
> Truncate table Employee
> insert into Employee (Id, Salary) values ('1', '100')
> insert into Employee (Id, Salary) values ('2', '200')
> insert into Employee (Id, Salary) values ('3', '300')
> ```
>
> 编写一个 SQL 查询，获取 Employee 表中第二高的薪水（Salary） 。
>
> ```
> +----+--------+
> | Id | Salary |
> +----+--------+
> | 1  | 100    |
> | 2  | 200    |
> | 3  | 300    |
> +----+--------+
> ```
>
>
> 例如上述 Employee 表，SQL查询应该返回 200 作为第二高的薪水。如果不存在第二高的薪水，那么查询应返回 null。
>
> ```
> +---------------------+
> | SecondHighestSalary |
> +---------------------+
> | 200                 |
> +---------------------+
> ```

```sql
#方法一：使用子查询和limit子句，将不同的薪资按降序排序后使用limit子句获得第二高的薪资
select
	distinct Salary as SecondHightestSalary 
	from Employee 
	order by Salary DESC 
	limit 1 offset 1;
#方法一的改进：如果不存在第二高工资的话，上面的方法会被判断为错误，因为表可能只有一项记录，为了克服这个问题我们可以将其作为临时表。
select 
	(select distinct Salary 
     from Employee 
     order by Salary DESC 
     limit 1 offset 1) as SecondHighestSalary;
#方法二：使用ifnull和limit子句,解决 “NULL” 问题的另一种方法是使用 “IFNULL” 函数
SELECT
    IFNULL(
      (SELECT DISTINCT Salary
       FROM Employee
       ORDER BY Salary DESC
        LIMIT 1 OFFSET 1),
    NULL) AS SecondHighestSalary；
```

### [第N高的薪水](https://leetcode-cn.com/problems/nth-highest-salary/)

> 编写一个 SQL 查询，获取 Employee 表中第 n 高的薪水（Salary）。
>
> ```
> +----+--------+
> | Id | Salary |
> +----+--------+
> | 1  | 100    |
> | 2  | 200    |
> | 3  | 300    |
> +----+--------+
> ```
>
>
> 例如上述 Employee 表，n = 2 时，应返回第二高的薪水 200。如果不存在第 n 高的薪水，那么查询应返回 null。
>
> ```
> +------------------------+
> | getNthHighestSalary(2) |
> +------------------------+
> | 200                    |
> +------------------------+
> ```

```sql
SELECT
    IFNULL(
      (SELECT DISTINCT Salary
       FROM Employee
       ORDER BY Salary DESC
        LIMIT 1 OFFSET n),
    NULL) AS SecondHighestSalary；
```

### [分数排名](https://leetcode-cn.com/problems/rank-scores/)

> sql架构：
>
> ```sql
> Create table If Not Exists Scores (Id int, Score DECIMAL(3,2))
> Truncate table Scores
> insert into Scores (Id, Score) values ('1', '3.5')
> insert into Scores (Id, Score) values ('2', '3.65')
> insert into Scores (Id, Score) values ('3', '4.0')
> insert into Scores (Id, Score) values ('4', '3.85')
> insert into Scores (Id, Score) values ('5', '4.0')
> insert into Scores (Id, Score) values ('6', '3.65')
> ```
>
> 编写一个 SQL 查询来实现分数排名。
>
> 如果两个分数相同，则两个分数排名（Rank）相同。请注意，平分后的下一个名次应该是下一个连续的整数值。换句话说，名次之间不应该有“间隔”。
>
> ```
> +----+-------+
> | Id | Score |
> +----+-------+
> | 1  | 3.50  |
> | 2  | 3.65  |
> | 3  | 4.00  |
> | 4  | 3.85  |
> | 5  | 4.00  |
> | 6  | 3.65  |
> +----+-------+
> ```
>
>
> 例如，根据上述给定的 Scores 表，你的查询应该返回（按分数从高到低排列）：
>
> ```
> +-------+------+
> | Score | Rank |
> +-------+------+
> | 4.00  | 1    |
> | 4.00  | 1    |
> | 3.85  | 2    |
> | 3.65  | 3    |
> | 3.65  | 3    |
> | 3.50  | 4    |
> +-------+------+
> ```
>
> 重要提示：对于 MySQL 解决方案，如果要转义用作列名的保留字，可以在关键字之前和之后使用撇号。例如 `Rank

```sql
#分为两个步骤，第一个是先将score排序，第二个是统计去重后的大于score的数量
select a.Score as Score,
(select count(distinct b.Score) from Scores b where b.Score >= a.Score) as Rank
from Scores a
order by a.Score DESC
```

### [连续出现的数字](https://leetcode-cn.com/problems/consecutive-numbers/)

> sql结构：
>
> ```sql
> Create table If Not Exists Logs (Id int, Num int)
> Truncate table Logs
> insert into Logs (Id, Num) values ('1', '1')
> insert into Logs (Id, Num) values ('2', '1')
> insert into Logs (Id, Num) values ('3', '1')
> insert into Logs (Id, Num) values ('4', '2')
> insert into Logs (Id, Num) values ('5', '1')
> insert into Logs (Id, Num) values ('6', '2')
> insert into Logs (Id, Num) values ('7', '2')
> ```
>
> 表：Logs
>
> ```
> +-------------+---------+
> | Column Name | Type    |
> +-------------+---------+
> | id          | int     |
> | num         | varchar |
> +-------------+---------+
> id 是这个表的主键。
> ```
>
> 编写一个 SQL 查询，查找所有至少连续出现三次的数字。
>
> 返回的结果表中的数据可以按 **任意顺序** 排列。查询结果格式如下面的例子所示：
>
> ```
> Logs 表：
> +----+-----+
> | Id | Num |
> +----+-----+
> | 1  | 1   |
> | 2  | 1   |
> | 3  | 1   |
> | 4  | 2   |
> | 5  | 1   |
> | 6  | 2   |
> | 7  | 2   |
> +----+-----+
> 
> Result 表：
> +-----------------+
> | ConsecutiveNums |
> +-----------------+
> | 1               |
> +-----------------+
> 1 是唯一连续出现至少三次的数字。
> ```

```sql

```

### [超过经理收入的员工](https://leetcode-cn.com/problems/employees-earning-more-than-their-managers/)

> sql架构：
>
> ```sql
> Create table If Not Exists Employee (Id int, Name varchar(255), Salary int, ManagerId int)
> Truncate table Employee
> insert into Employee (Id, Name, Salary, ManagerId) values ('1', 'Joe', '70000', '3')
> insert into Employee (Id, Name, Salary, ManagerId) values ('2', 'Henry', '80000', '4')
> insert into Employee (Id, Name, Salary, ManagerId) values ('3', 'Sam', '60000', 'None')
> insert into Employee (Id, Name, Salary, ManagerId) values ('4', 'Max', '90000', 'None')
> ```
>
> Employee 表包含所有员工，他们的经理也属于员工。每个员工都有一个 Id，此外还有一列对应员工的经理的 Id。
>
> ```
> +----+-------+--------+-----------+
> | Id | Name  | Salary | ManagerId |
> +----+-------+--------+-----------+
> | 1  | Joe   | 70000  | 3         |
> | 2  | Henry | 80000  | 4         |
> | 3  | Sam   | 60000  | NULL      |
> | 4  | Max   | 90000  | NULL      |
> +----+-------+--------+-----------+
> ```
>
>
> 给定 Employee 表，编写一个 SQL 查询，该查询可以获取收入超过他们经理的员工的姓名。在上面的表格中，Joe 是唯一一个收入超过他的经理的员工。
>
> ```
> +----------+
> | Employee |
> +----------+
> | Joe      |
> +----------+
> ```

```sql
SELECT
    a.Name AS 'Employee'
FROM
    Employee AS a,
    Employee AS b
WHERE
    a.ManagerId = b.Id
        AND a.Salary > b.Salary
;
```

### [ 查找重复的电子邮箱](https://leetcode-cn.com/problems/duplicate-emails/)

> sql架构：
>
> ```sql
> Create table If Not Exists Person (Id int, Email varchar(255))
> Truncate table Person
> insert into Person (Id, Email) values ('1', 'a@b.com')
> insert into Person (Id, Email) values ('2', 'c@d.com')
> insert into Person (Id, Email) values ('3', 'a@b.com')
> ```
>
> 编写一个 SQL 查询，查找 Person 表中所有重复的电子邮箱。
>
> 示例：
>
> ```
> +----+---------+
> | Id | Email   |
> +----+---------+
> | 1  | a@b.com |
> | 2  | c@d.com |
> | 3  | a@b.com |
> +----+---------+
> ```
>
>
> 根据以上输入，你的查询应返回以下结果：
>
> ```
> +---------+
> | Email   |
> +---------+
> | a@b.com |
> +---------+
> ```
>
> 说明：所有电子邮箱都是小写字母。

```sql
#方法一
select Email from
(
  select Email, count(Email) as num
  from Person
  group by Email
) as statistic
where num > 1
;

#方法二
select Email
from Person
group by Email
having count(Email) > 1;
```



